package GUI;
import org.mindrot.jbcrypt.BCrypt;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import javax.swing.*;
import DAO.UserDAO;
import DAO.actionDAO;
import MODEL.User;

public class AddUsersGUI {

    private JFrame frame;
    private JTextField textFieldNom;
    private JTextField textFieldPrenom;
    private JTextField textFieldMail;
    private JPasswordField passwordField;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    AddUsersGUI window = new AddUsersGUI(null);
                    window.frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the application.
     */
    public AddUsersGUI(User connectedUser) throws SQLException {
        initialize(connectedUser);
        this.frame.setVisible(true);
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize(User connectedUser) throws SQLException {
        frame = new JFrame();
        frame.setBounds(100, 100, 800, 600);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setBackground(Color.WHITE);
        frame.getContentPane().setLayout(null);
        frame.setLocationRelativeTo(null); // Center the window
        frame.setTitle("Ajouter un utilisateur");

        JLabel lblTitle = new JLabel("AJOUTER UN ADMIN");
        lblTitle.setFont(new Font("Segoe UI", Font.BOLD, 24));
        lblTitle.setForeground(new Color(22, 100, 255));
        lblTitle.setBounds(200, 20, 400, 30);
        frame.getContentPane().add(lblTitle);

        JLabel lblNom = new JLabel("Nom:");
        lblNom.setFont(new Font("Segoe UI", Font.PLAIN, 16));
        lblNom.setForeground(new Color(22, 100, 255));
        lblNom.setBounds(150, 80, 100, 30);
        frame.getContentPane().add(lblNom);

        textFieldNom = new JTextField();
        textFieldNom.setFont(new Font("Segoe UI", Font.PLAIN, 16));
        textFieldNom.setBounds(300, 80, 300, 30);
        frame.getContentPane().add(textFieldNom);
        textFieldNom.setColumns(10);

        JLabel lblPrenom = new JLabel("Prénom:");
        lblPrenom.setFont(new Font("Segoe UI", Font.PLAIN, 16));
        lblPrenom.setForeground(new Color(22, 100, 255));
        lblPrenom.setBounds(150, 130, 100, 30);
        frame.getContentPane().add(lblPrenom);

        textFieldPrenom = new JTextField();
        textFieldPrenom.setFont(new Font("Segoe UI", Font.PLAIN, 16));
        textFieldPrenom.setBounds(300, 130, 300, 30);
        frame.getContentPane().add(textFieldPrenom);
        textFieldPrenom.setColumns(10);

        JLabel lblEmail = new JLabel("Email:");
        lblEmail.setFont(new Font("Segoe UI", Font.PLAIN, 16));
        lblEmail.setForeground(new Color(22, 100, 255));
        lblEmail.setBounds(150, 180, 100, 30);
        frame.getContentPane().add(lblEmail);

        textFieldMail = new JTextField();
        textFieldMail.setFont(new Font("Segoe UI", Font.PLAIN, 16));
        textFieldMail.setBounds(300, 180, 300, 30);
        frame.getContentPane().add(textFieldMail);
        textFieldMail.setColumns(10);

        JLabel lblPassword = new JLabel("Mot de passe:");
        lblPassword.setFont(new Font("Segoe UI", Font.PLAIN, 16));
        lblPassword.setForeground(new Color(22, 100, 255));
        lblPassword.setBounds(150, 230, 150, 30);
        frame.getContentPane().add(lblPassword);

        passwordField = new JPasswordField();
        passwordField.setFont(new Font("Segoe UI", Font.PLAIN, 16));
        passwordField.setBounds(300, 230, 300, 30);
        frame.getContentPane().add(passwordField);

        JButton btnCreer = new JButton("Créer");
        btnCreer.setFont(new Font("Segoe UI", Font.PLAIN, 16));
        btnCreer.setBackground(new Color(22, 100, 255));
        btnCreer.setForeground(Color.WHITE);
        btnCreer.setBounds(250, 330, 120, 40);
        btnCreer.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                UserDAO u = new UserDAO();
                String nom = textFieldNom.getText();
                String prenom = textFieldPrenom.getText();
                String email = textFieldMail.getText();
                String mdp = new String(passwordField.getPassword());
                String hashedPassword = BCrypt.hashpw(mdp, BCrypt.gensalt());

                User user = new User(nom, prenom, hashedPassword, email, "Admin");
                int returnValue = u.add(user);
                if (returnValue == 1) {
                    JOptionPane.showMessageDialog(frame, "Nouvel administrateur ajouté ");
                    textFieldNom.setText("");
                    textFieldPrenom.setText("");
                    textFieldMail.setText("");
                    passwordField.setText("");
                    
                   actionDAO a = new actionDAO();
                   try {
					a.insertAction(connectedUser.getId(), "Ajout admin", email);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
                } else {
                    JOptionPane.showMessageDialog(frame, "Échec de l'ajout de l'administrateur!", "Erreur", JOptionPane.WARNING_MESSAGE);
                }
            }
        });
        frame.getContentPane().add(btnCreer);

        JButton btnAnnuler = new JButton("Retour");
        btnAnnuler.setFont(new Font("Segoe UI", Font.PLAIN, 16));
        btnAnnuler.setBackground(new Color(255, 69, 58)); // Red color for cancel
        btnAnnuler.setForeground(Color.WHITE);
        btnAnnuler.setBounds(430, 330, 120, 40);
        btnAnnuler.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                frame.dispose();
                try {
                    AdminGUI adminGUI = new AdminGUI(connectedUser);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        });
        frame.getContentPane().add(btnAnnuler);
    }
}
