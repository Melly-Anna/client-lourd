package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import DAO.UserDAO;
import DAO.actionDAO;
import DAO.inscriptionDAO;
import GUI.userssGUI.ButtonEditor;
import GUI.userssGUI.ButtonRenderer;
import MODEL.User;
import MODEL.action;
import MODEL.inscription;

public class Actions {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Actions window = new Actions();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Actions() {
		initialize();
		 this.frame.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setSize(800, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);

        JPanel controlPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        controlPanel.setBackground(Color.gray);
        frame.getContentPane().add(controlPanel, BorderLayout.NORTH);
        
     // modèle de table 
        DefaultTableModel model = new DefaultTableModel();
        model.addColumn("Id Administrateur");
        model.addColumn("Actions");
        model.addColumn("Utlisateur concerné");
        model.addColumn("Date");

        JComboBox roleComboBox = new JComboBox<>();
        
        roleComboBox.addItem("Toutes les actions");
        roleComboBox.addItem("Ajout admin");
        roleComboBox.addItem("Reinitialisation mot de passe admin");
        roleComboBox.addItem("Reinitialisation mot de passe utilisateur");
        roleComboBox.addItem("Suppression d'un Administrateur");
        roleComboBox.addItem("Suppression d'un utilisateur");
        roleComboBox.addItem("Validation inscription");
        roleComboBox.addItem("Refus inscription");
        
        
        controlPanel.add(roleComboBox);

    // écouteur d'événements sur le JComboBox
        roleComboBox.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                
                filterbyAction((String) roleComboBox.getSelectedItem(),model);
            }
        });
        
        JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        
        JButton returnButton = new JButton("Retour");
        returnButton.setPreferredSize(new Dimension(120, 30)); // Taille du bouton Retour
        returnButton.setBackground(Color.RED);
        returnButton.setForeground(Color.WHITE);
        returnButton.addActionListener(e -> {
            frame.dispose();
            try {
                AdminGUI adminGUI = new AdminGUI(null);
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        });
        buttonPanel.add(returnButton);

        JScrollPane scrollPane = new JScrollPane();
        frame.getContentPane().add(scrollPane, BorderLayout.CENTER);

        frame.getContentPane().add(buttonPanel, BorderLayout.SOUTH);
      

   


        JTable table = new JTable(model);
        scrollPane.setViewportView(table);
        
        


        loadAllAction(model);
        
	}
	
	private void addRowToTable(action act,DefaultTableModel model) {
        model.addRow(new Object[]{act.getId_admin(), act.getAction(), act.getId_user(), act.getDate()});
    }
    private void loadAllAction(DefaultTableModel model) {
        model.setRowCount(0);
        try {
            actionDAO a = new actionDAO();
            ArrayList<action> act = a.getAction();
            for (action actions : act) {
                addRowToTable(actions,model);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
    private void filterbyAction(String action,DefaultTableModel model) {
        try {
        	model.setRowCount(0);
        	 actionDAO a = new actionDAO();
             ArrayList<action> act = a.filtreAction(action);
             for (action actions : act) {
                 addRowToTable(actions, model);
                }
            
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
	
}
