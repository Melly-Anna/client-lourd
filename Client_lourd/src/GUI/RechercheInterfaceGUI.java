package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableModel;

import DAO.Recherche1DAO;
import MODEL.Recherche1;

public class RechercheInterfaceGUI extends JFrame {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTable table;
    private DefaultTableModel tableModel;
    private JTextField dateField;
    private Recherche1DAO r1DAO;

    public RechercheInterfaceGUI() {
        r1DAO = new Recherche1DAO();
        initializeUI();
        this.setVisible(true);
    }

    private void initializeUI() {
        setTitle("Affichage des Recherches");
        setSize(800, 600);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setLayout(new BorderLayout());

        // Table model
        tableModel = new DefaultTableModel();
        tableModel.setColumnIdentifiers(new String[]{"Date", "Adresse IP", "Région", "Code Postal", "Fédération", "idrecherche"});
        table = new JTable(tableModel);
        add(new JScrollPane(table), BorderLayout.CENTER);

        // Panel de filtre et boutons
        JPanel panel = new JPanel();
        panel.setLayout(new FlowLayout());

        panel.add(new JLabel("Date (YYYY-MM-DD):"));
        dateField = new JTextField(10);
        panel.add(dateField);

        JButton filterButton = new JButton("Filtrer");
        filterButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                filterResults();
            }
        });
        panel.add(filterButton);

        JButton downloadButton = new JButton("Télécharger");
        downloadButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                downloadResults();
            }
        });
        panel.add(downloadButton);
        
        JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        
        JButton returnButton = new JButton("Retour");
        returnButton.setPreferredSize(new Dimension(120, 30)); // Taille du bouton Retour
        returnButton.setBackground(Color.RED);
        returnButton.setForeground(Color.WHITE);
        returnButton.addActionListener(e -> {
            dispose();
            try {
                AdminGUI adminGUI = new AdminGUI(null);
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        });
        buttonPanel.add(returnButton);

        add(panel, BorderLayout.NORTH);
        add(buttonPanel, BorderLayout.SOUTH);
        // Charger les données initiales
        loadAllResults();

        setVisible(true);
    }

    private void loadAllResults() {
        try {
            List<Recherche1> recherches = r1DAO.getAllRecherches();
            updateTable(recherches);
        } catch (SQLException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(this, "Erreur lors du chargement des résultats : " + e.getMessage());
        }
    }

    private void filterResults() {
        String date = dateField.getText().trim();
        try {
            List<Recherche1> recherches = r1DAO.getRecherchesByDate(date);
            updateTable(recherches);
        } catch (SQLException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(this, "Erreur lors du filtrage des résultats : " + e.getMessage());
        }
    }

    private void updateTable(List<Recherche1> recherches) {
        tableModel.setRowCount(0);
        for (Recherche1 recherche : recherches) {
            tableModel.addRow(new Object[]{
                 
                    recherche.getDateConnexion(),
                    recherche.getAdresseIp(),
                    recherche.getRegion(),
                    recherche.getCodePostal(),
                    recherche.getFederation()
            });
        }
    }

    private void downloadResults() {
    	String currentDirectory = System.getProperty("user.dir");
    	try (FileWriter writer = new FileWriter("recherche.log")) {
            for (int i = 0; i < tableModel.getRowCount(); i++) {
                for (int j = 0; j < tableModel.getColumnCount(); j++) {
                    writer.write(tableModel.getValueAt(i, j) + " ");
                }
                writer.write("\n");
            }
            JOptionPane.showMessageDialog(this, "Les résultats ont été téléchargés dans recherche.log");
            System.out.println("Le répertoire de travail actuel est : " + currentDirectory);
        } catch (IOException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(this, "Erreur lors du téléchargement des résultats : " + e.getMessage());
        }
    }

    public static void main(String[] args){
    	  SwingUtilities.invokeLater(() -> {
          new RechercheInterfaceGUI();
          
        });
    }
}
