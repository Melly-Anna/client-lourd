package GUI;

import DAO.ClubDAO;
import MODEL.Club;
import MODEL.User;

import com.formdev.flatlaf.FlatLightLaf;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ClubSearchGUI extends JFrame {

    private ClubDAO clubDAO;
    private JComboBox<String> filterComboBox;
    private JComboBox<String> choiceComboBox;
    private JComboBox<String> federationComboBox;
    private JTable resultTable;
    private DefaultTableModel tableModel;

    public ClubSearchGUI(User connectedUser) {
        clubDAO = new ClubDAO();
        initComponents(connectedUser);
    }

    private void initComponents(User connectedUser) {
        try {
            UIManager.setLookAndFeel(new FlatLightLaf());
        } catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }

        setTitle("Recherche de Clubs");
        setSize(800, 600);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);

        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
        add(mainPanel);

        // Title Label
        JLabel titleLabel = new JLabel("Recherche de Clubs");
        titleLabel.setFont(new Font("Segoe UI", Font.BOLD, 24));
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        mainPanel.add(titleLabel);

        JPanel filterPanel = new JPanel();
        filterPanel.add(new JLabel("Choix du filtre:"));
        String[] filters = {"Region", "Commune", "Departement"};
        filterComboBox = new JComboBox<>(filters);
        filterComboBox.addActionListener(e -> handleFilterSelection());
        filterPanel.add(filterComboBox);
        mainPanel.add(filterPanel);

        choiceComboBox = new JComboBox<>();
        JPanel choicePanel = new JPanel();
        choicePanel.add(new JLabel("Choix:"));
        choicePanel.add(choiceComboBox);
        mainPanel.add(choicePanel);

        federationComboBox = new JComboBox<>();
        JPanel federationPanel = new JPanel();
        federationPanel.add(new JLabel("Fédération:"));
        federationPanel.add(federationComboBox);
        mainPanel.add(federationPanel);

        JButton searchButton = new JButton("Rechercher");
        searchButton.setFont(new Font("Segoe UI", Font.PLAIN, 16));
        searchButton.setForeground(Color.WHITE);
        searchButton.setBackground(new Color(22, 100, 255));
        searchButton.addActionListener(e -> searchClubs());
        mainPanel.add(searchButton);

        // Spacing panel
        mainPanel.add(Box.createRigidArea(new Dimension(0, 10)));

        tableModel = new DefaultTableModel();
        resultTable = new JTable(tableModel) {
            // Override isCellEditable method to make cells non-editable
            @Override
            public boolean isCellEditable(int row, int column) {
                return false; // Make all cells non-editable
            }
        };
        JScrollPane scrollPane = new JScrollPane(resultTable);
        mainPanel.add(scrollPane);

        // Spacing panel
        mainPanel.add(Box.createRigidArea(new Dimension(0, 10)));

        JButton returnButton = new JButton("Retour");
        returnButton.setFont(new Font("Segoe UI", Font.PLAIN, 16));
        returnButton.setForeground(Color.WHITE);
        returnButton.setBackground(Color.RED);
        returnButton.addActionListener(e -> {
        	dispose(); // Ferme la fenêtre actuelle
            try {
				AdminGUI adminGUI = new AdminGUI(connectedUser);
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} // Instancie la fenêtre AdminGUI
            
        });
        mainPanel.add(returnButton);
        
        Color lightBlue = new Color(173, 216, 230); // Couleur bleu clair
        Color darkBlue = new Color(0, 102, 204);    // Couleur bleu foncé

        // Modifier les couleurs des composants graphiques
        mainPanel.setBackground(lightBlue);  // Fond du panneau principal
        titleLabel.setForeground(darkBlue); // Couleur du titre



        populateFederationComboBox();
        handleFilterSelection();
    }

    private void handleFilterSelection() {
        String selectedFilter = filterComboBox.getSelectedItem().toString();
        switch (selectedFilter) {
            case "Region":
                populateChoiceComboBox(clubDAO.getAllRegions());
                break;
            case "Commune":
                populateChoiceComboBox(clubDAO.getAllCommunes());
                break;
            case "Departement":
                populateChoiceComboBox(clubDAO.getAllDepartements());
                break;
        }
        populateFederationComboBox();
    }

    private void populateChoiceComboBox(List<String> items) {
        Collections.sort(items);
        choiceComboBox.removeAllItems();
        for (String item : items) {
            choiceComboBox.addItem(item);
        }
    }

    private void populateFederationComboBox() {
        List<String> federations = clubDAO.getAllFederations();
        Collections.sort(federations);
        federationComboBox.removeAllItems();
        for (String federation : federations) {
            federationComboBox.addItem(federation);
        }
    }

    private void searchClubs() {
        String selectedFilter = filterComboBox.getSelectedItem().toString();
        String choice = choiceComboBox.getSelectedItem().toString();
        String federation = federationComboBox.getSelectedItem().toString();

        List<Club> clubs = clubDAO.getClubsByFilters(selectedFilter, choice, federation);
        displayClubs(clubs);
    }

    private void displayClubs(List<Club> clubs) {
        Collections.sort(clubs, Comparator.comparing(Club::getCommune));

        tableModel.setRowCount(0); // Clear the table
        tableModel.setColumnCount(0); // Clear the columns

        // Add columns to the table
        tableModel.addColumn("Commune");
        tableModel.addColumn("Département");
        tableModel.addColumn("Région");
        tableModel.addColumn("Code");
        tableModel.addColumn("Fédération");
        tableModel.addColumn("Clubs");
        tableModel.addColumn("EPA");
        tableModel.addColumn("Total");

        if (clubs.isEmpty()) {
            JOptionPane.showMessageDialog(this, "Aucun résultat", "Résultats de la recherche", JOptionPane.INFORMATION_MESSAGE);
        } else {
            for (Club club : clubs) {
                Object[] row = {club.getCommune(), club.getDepartement(), club.getRegion(), club.getCode(), club.getFederation(), club.getClubs(), club.getEpa(), club.getTotal()};
                tableModel.addRow(row);
            }
        }
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            ClubSearchGUI clubSearchGUI = new ClubSearchGUI(null);
            clubSearchGUI.setVisible(true);
        });
    }
}
