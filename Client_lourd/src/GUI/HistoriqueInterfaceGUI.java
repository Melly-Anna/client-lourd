package GUI;

import com.formdev.flatlaf.FlatLightLaf;
import MODEL.User;
import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.SqlDateModel;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;

public class HistoriqueInterfaceGUI extends JFrame {

    private JPanel mainPanel;
    private JTable table;
    private DefaultTableModel tableModel;
    private Connection connection;
    private JDatePickerImpl startDatePicker;
    private JDatePickerImpl endDatePicker;
    private JCheckBox allCheckBox;

    public HistoriqueInterfaceGUI(User connectedUser) {
        try {
            UIManager.setLookAndFeel(new FlatLightLaf());
        } catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }

        setTitle("Historique de Connexion");
        setSize(1200, 800);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);

        mainPanel = new JPanel(new BorderLayout());
        add(mainPanel);

        // Panel pour les sélecteurs de date et les filtres de période
        JPanel filterPanel = new JPanel(new FlowLayout());

        // Configuration des modèles de date
        SqlDateModel startDateModel = new SqlDateModel();
        Properties p = new Properties();
        p.put("text.today", "Today");
        p.put("text.month", "Month");
        p.put("text.year", "Year");
        JDatePanelImpl startDatePanel = new JDatePanelImpl(startDateModel, p);
        startDatePicker = new JDatePickerImpl(startDatePanel, new DateLabelFormatter("yyyy-MM-dd'T'HH:mm:ss"));
        filterPanel.add(new JLabel("Date de début:"));
        filterPanel.add(startDatePicker);

        SqlDateModel endDateModel = new SqlDateModel();
        JDatePanelImpl endDatePanel = new JDatePanelImpl(endDateModel, p);
        endDatePicker = new JDatePickerImpl(endDatePanel, new DateLabelFormatter("yyyy-MM-dd'T'HH:mm:ss"));
        filterPanel.add(new JLabel("Date de fin:"));
        filterPanel.add(endDatePicker);

        JButton filterButton = new JButton("Filtrer");
        filterButton.addActionListener(e -> filterData());
        filterPanel.add(filterButton);

        // Ajouter les filtres de période
        JCheckBox last24HoursCheckBox = new JCheckBox("Dernières 24h");
        last24HoursCheckBox.addActionListener(e -> filterByPeriod("24h"));
        JCheckBox lastWeekCheckBox = new JCheckBox("Dernière semaine");
        lastWeekCheckBox.addActionListener(e -> filterByPeriod("week"));
        JCheckBox last3MonthsCheckBox = new JCheckBox("3 derniers mois");
        last3MonthsCheckBox.addActionListener(e -> filterByPeriod("3months"));
        allCheckBox = new JCheckBox("Tout");
        allCheckBox.addActionListener(e -> filterByPeriod("all"));

        filterPanel.add(last24HoursCheckBox);
        filterPanel.add(lastWeekCheckBox);
        filterPanel.add(last3MonthsCheckBox);
        filterPanel.add(allCheckBox);

        mainPanel.add(filterPanel, BorderLayout.NORTH);

        // Connexion à la base de données et initialisation du tableau
        try {
            String url = "jdbc:mysql://localhost:3306/club_de_sport";
            String user = "root";
            String password = "root";
            connection = DriverManager.getConnection(url, user, password);

            // Récupération des données de la table historique
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT id_users, adresse_ip, date_connexion, navigateur, statut_connexion " +
                    "FROM club_de_sport.historique " +
                    "ORDER BY date_connexion DESC");

            // Création d'une JTable pour afficher les données
            table = new JTable(buildTableModel(resultSet)) {
                @Override
                public boolean isCellEditable(int row, int column) {
                    return false; // Make all cells non-editable
                }
            };
            table.setGridColor(Color.BLACK);

            // Ajout de la JTable à un JScrollPane
            JScrollPane scrollPane = new JScrollPane(table);
            mainPanel.add(scrollPane, BorderLayout.CENTER);

        } catch (SQLException e) {
            e.printStackTrace();
        }

        // Panel pour les boutons Retour et Statistiques
        JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        
        JButton returnButton = new JButton("Retour");
        returnButton.setPreferredSize(new Dimension(120, 30)); // Taille du bouton Retour
        returnButton.setBackground(Color.RED);
        returnButton.setForeground(Color.WHITE);
        returnButton.addActionListener(e -> {
            dispose();
            try {
                AdminGUI adminGUI = new AdminGUI(connectedUser);
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        });
        buttonPanel.add(returnButton);

        JButton statsButton = new JButton("Statistiques");
        statsButton.setPreferredSize(new Dimension(120, 30)); // Taille du bouton Statistiques
        statsButton.setBackground(Color.BLUE);
        statsButton.setForeground(Color.WHITE);
        statsButton.addActionListener(e -> {
            dispose();
            try {
                StatistiquesGUI statistiquesGUI = new StatistiquesGUI();
                statistiquesGUI.setVisible(true);
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        });
        buttonPanel.add(statsButton);
        
        JButton downloadButton = new JButton("Télécharger");
        downloadButton.setPreferredSize(new Dimension(120, 30)); // Taille du bouton Télécharger
        downloadButton.setBackground(Color.GREEN);
        downloadButton.setForeground(Color.WHITE);
        downloadButton.addActionListener(e -> downloadResults());
        buttonPanel.add(downloadButton);

        mainPanel.add(buttonPanel, BorderLayout.SOUTH);

        setVisible(true);
    }

    // Méthode pour convertir un ResultSet en DefaultTableModel
    public static DefaultTableModel buildTableModel(ResultSet resultSet) throws SQLException {
        DefaultTableModel tableModel = new DefaultTableModel();
        ResultSetMetaData metaData = resultSet.getMetaData();
        int columnCount = metaData.getColumnCount();
        for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
            tableModel.addColumn(metaData.getColumnName(columnIndex));
        }
        while (resultSet.next()) {
            Object[] row = new Object[columnCount];
            for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
                row[columnIndex - 1] = resultSet.getObject(columnIndex);
            }
            tableModel.addRow(row);
        }
        return tableModel;
    }

    // Méthode pour filtrer les données par date
    private void filterData() {
        java.sql.Date startDate = (java.sql.Date) startDatePicker.getModel().getValue();
        java.sql.Date endDate = (java.sql.Date) endDatePicker.getModel().getValue();

        String query = "SELECT id_users, adresse_ip, date_connexion, navigateur, statut_connexion " +
                "FROM club_de_sport.historique ";

        if (startDate != null && endDate != null) {
            query += "WHERE date_connexion BETWEEN '" + new Timestamp(startDate.getTime()) + "' AND '" + new Timestamp(endDate.getTime()) + "' ";
        }

        query += "ORDER BY date_connexion DESC";

        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            table.setModel(buildTableModel(resultSet));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void filterByPeriod(String period) {
        String query = "SELECT id_users, adresse_ip, date_connexion, navigateur, statut_connexion " +
                "FROM club_de_sport.historique ";

        switch (period) {
            case "24h":
                query += "WHERE date_connexion >= NOW() - INTERVAL 1 DAY ";
                break;
            case "week":
                query += "WHERE date_connexion >= NOW() - INTERVAL 1 WEEK ";
                break;
            case "3months":
                query += "WHERE date_connexion >= NOW() - INTERVAL 3 MONTH ";
                break;
            case "all":
                // No additional conditions
                break;
        }

        query += "ORDER BY date_connexion DESC";

        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            table.setModel(buildTableModel(resultSet));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void showGUI() {
        setVisible(true);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            HistoriqueInterfaceGUI historiqueInterfaceGUI = new HistoriqueInterfaceGUI(null);
            historiqueInterfaceGUI.showGUI();
        });
    }

    // Classe pour formater les dates dans les sélecteurs de date
    public static class DateLabelFormatter extends JFormattedTextField.AbstractFormatter {
        private final String datePattern;
        private final SimpleDateFormat dateFormatter;

        public DateLabelFormatter(String datePattern) {
            this.datePattern = datePattern;
            this.dateFormatter = new SimpleDateFormat(datePattern);
        }

        @Override
        public Object stringToValue(String text) throws ParseException {
            return dateFormatter.parseObject(text);
        }

        @Override
        public String valueToString(Object value) {
            if (value != null) {
                Calendar cal = (Calendar) value;
                return dateFormatter.format(cal.getTime());
            }
            return "";
        }
    }


    private void downloadResults() {
    	try (FileWriter writer = new FileWriter("connexion.log")) {
            // Écrire les en-têtes de colonnes
            for (int i = 0; i < table.getColumnCount(); i++) {
                writer.write(table.getColumnName(i) + "\t");
            }
            writer.write("\n");

            // Écrire les données des lignes
            for (int i = 0; i < table.getRowCount(); i++) {
                for (int j = 0; j < table.getColumnCount(); j++) {
                    Object value = table.getValueAt(i, j);
                    writer.write((value != null ? value.toString() : "") + "\t");
                }
                writer.write("\n");
            }
            JOptionPane.showMessageDialog(this, "Données téléchargées avec succès dans connexion.log");
        } catch (IOException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(this, "Erreur lors de l'enregistrement du fichier", "Erreur", JOptionPane.ERROR_MESSAGE);
        }
    }
}


