package GUI;

import javax.swing.*;
import java.awt.*;
import java.sql.SQLException;
import java.util.List;
import DAO.HistoriqueDAO;
import MODEL.Historique;
import MODEL.User;

public class NotificationsGUI extends JFrame {
    private User connectedUser;

    public NotificationsGUI(User connectedUser) throws SQLException {
        this.connectedUser = connectedUser;
        initialize();
    }

    private void initialize() throws SQLException {
        setTitle("Notifications");
        setBounds(100, 100, 400, 300);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);
        getContentPane().setLayout(new BorderLayout());

        HistoriqueDAO historiqueDAO = new HistoriqueDAO();
        List<Historique> failedAttempts = historiqueDAO.getFailedLoginAttempts();
        
        DefaultListModel<String> listModel = new DefaultListModel<>();
        for (Historique attempt : failedAttempts) {
            String notification = "User ID: " + attempt.getIdUser() + " failed to login on " + attempt.getDateConnexion();
            listModel.addElement(notification);
        }

        JList<String> notificationList = new JList<>(listModel);
        JScrollPane scrollPane = new JScrollPane(notificationList);
        getContentPane().add(scrollPane, BorderLayout.CENTER);

        JButton btnClose = new JButton("Close");
        btnClose.addActionListener(e -> dispose());
        getContentPane().add(btnClose, BorderLayout.SOUTH);
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            try {
                NotificationsGUI window = new NotificationsGUI(null); // Pass the actual connected user here
                window.setVisible(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }
}
