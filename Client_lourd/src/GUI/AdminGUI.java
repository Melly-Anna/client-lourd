package GUI;
 
import com.formdev.flatlaf.FlatLightLaf;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.List;
import DAO.HistoriqueDAO;
import DAO.UserDAO;
import MODEL.Historique;
import MODEL.User;
 
public class AdminGUI {
 
    private JFrame frame;
    private int unreadNotifications;
 
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    FlatLightLaf.install();
                    UIManager.setLookAndFeel(new FlatLightLaf());
                    AdminGUI window = new AdminGUI(null);
                    window.frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
 
    public AdminGUI(User connectedUser) throws SQLException {
        initialize(connectedUser);
        this.frame.setVisible(true);
    }
 
    private void initialize(User connectedUser) throws SQLException {
        // Récupération des tentatives de connexion échouées
        HistoriqueDAO historiqueDAO = new HistoriqueDAO();
        List<Historique> failedAttempts = historiqueDAO.getFailedLoginAttempts();
        unreadNotifications = failedAttempts.size(); // Définir la valeur de unreadNotifications
 
        UserDAO u = new UserDAO();
        frame = new JFrame("Plateforme Administrateur - Club Sportif");
        frame.getContentPane().setBackground(Color.WHITE); // White background
        frame.setBounds(100, 100, 800, 600);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);
        frame.setLocationRelativeTo(null); // Centre la fenêtre à l'écran
 
        // Déconnexion Button
        JButton btnDeconnexion = new JButton("Déconnexion");
        btnDeconnexion.setFont(new Font("Segoe UI", Font.PLAIN, 14));
        btnDeconnexion.setBackground(new Color(255, 69, 58)); // Red color for logout
        btnDeconnexion.setForeground(Color.WHITE);
        btnDeconnexion.setFocusPainted(false);
        btnDeconnexion.setBounds(10, 10, 120, 30);
        btnDeconnexion.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                // Log out logic
                frame.dispose();
                new ConnexionAdminGUI();
            }
        });
        frame.getContentPane().add(btnDeconnexion);
 
        JLabel lblTitle = new JLabel("PLATEFORME ADMINISTRATEUR");
        lblTitle.setFont(new Font("Segoe UI", Font.BOLD, 24)); // Larger and bold font
        lblTitle.setForeground(new Color(22, 100, 255)); // Set font color
        lblTitle.setBounds(200, 36, 400, 30); // Centered title
        frame.getContentPane().add(lblTitle);
 
        JLabel lblNom = new JLabel("Nom:");
        lblNom.setFont(new Font("Segoe UI", Font.PLAIN, 16));
        lblNom.setForeground(new Color(22, 100, 255));
        lblNom.setBounds(250, 120, 100, 30);
        frame.getContentPane().add(lblNom);
 
        JLabel lblNomValue = new JLabel(connectedUser != null ? connectedUser.getNom() : "Admin");
        lblNomValue.setFont(new Font("Segoe UI", Font.PLAIN, 16));
        lblNomValue.setBounds(350, 120, 200, 30);
        frame.getContentPane().add(lblNomValue);
 
        JLabel lblPrenom = new JLabel("Prénom:");
        lblPrenom.setFont(new Font("Segoe UI", Font.PLAIN, 16));
        lblPrenom.setForeground(new Color(22, 100, 255));
        lblPrenom.setBounds(250, 160, 100, 30);
        frame.getContentPane().add(lblPrenom);
 
        JLabel lblPrenomValue = new JLabel(connectedUser != null ? connectedUser.getPrenom() : "Admin");
        lblPrenomValue.setFont(new Font("Segoe UI", Font.PLAIN, 16));
        lblPrenomValue.setBounds(350, 160, 200, 30);
        frame.getContentPane().add(lblPrenomValue);
 
        JButton btnGererUtilisateurs = new JButton("Gérer les utilisateurs");
        btnGererUtilisateurs.setFont(new Font("Segoe UI", Font.PLAIN, 16));
        btnGererUtilisateurs.setBackground(new Color(22, 100, 255));
        btnGererUtilisateurs.setForeground(Color.WHITE);
        btnGererUtilisateurs.setBounds(82, 212, 300, 40);
        btnGererUtilisateurs.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                frame.dispose();
                new userssGUI( connectedUser);
            }
        });
        frame.getContentPane().add(btnGererUtilisateurs);
 
        JButton btnCreerCompteUtilisateur = new JButton("Créer un compte utilisateur");
        btnCreerCompteUtilisateur.setFont(new Font("Segoe UI", Font.PLAIN, 16));
        btnCreerCompteUtilisateur.setBackground(new Color(22, 100, 255));
        btnCreerCompteUtilisateur.setForeground(Color.WHITE);
        btnCreerCompteUtilisateur.setBounds(82, 302, 300, 40);
        btnCreerCompteUtilisateur.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                frame.dispose();
                try {
                    new AddUsersGUI(connectedUser);
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
        });
        frame.getContentPane().add(btnCreerCompteUtilisateur);
 
        JButton btnGererClubs = new JButton("Gérer les clubs");
        btnGererClubs.setFont(new Font("Segoe UI", Font.PLAIN, 16));
        btnGererClubs.setBackground(new Color(22, 100, 255));
        btnGererClubs.setForeground(Color.WHITE);
        btnGererClubs.setBounds(417, 212, 300, 40);
        btnGererClubs.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                frame.dispose();
                ClubSearchGUI clubSearchGUI = new ClubSearchGUI(connectedUser);
                clubSearchGUI.setVisible(true);
            }
        });
        frame.getContentPane().add(btnGererClubs);
 
        JButton btnVoirHistorique = new JButton("Visualiser les historiques");
        btnVoirHistorique.setFont(new Font("Segoe UI", Font.PLAIN, 16));
        btnVoirHistorique.setBackground(new Color(22, 100, 255));
        btnVoirHistorique.setForeground(Color.WHITE);
        btnVoirHistorique.setBounds(82, 399, 300, 40);
        btnVoirHistorique.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                frame.dispose();
                new HistoriqueInterfaceGUI(connectedUser);
            }
        });
        frame.getContentPane().add(btnVoirHistorique);
 
        JButton btnGererInscriptions = new JButton("Gérer les inscriptions");
        btnGererInscriptions.setFont(new Font("Segoe UI", Font.PLAIN, 16));
        btnGererInscriptions.setBackground(new Color(22, 100, 255));
        btnGererInscriptions.setForeground(Color.WHITE);
        btnGererInscriptions.setBounds(417, 302, 300, 40);
        btnGererInscriptions.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                frame.dispose();
                new validerInscription(connectedUser);
            }
        });
        frame.getContentPane().add(btnGererInscriptions);
 
        // Notification button
        JButton btnNotifications = new JButton("Notifications");
        btnNotifications.setFont(new Font("Segoe UI", Font.PLAIN, 16));
        btnNotifications.setBackground(new Color(22, 100, 255));
        btnNotifications.setForeground(Color.WHITE);
        btnNotifications.setBounds(650, 10, 140, 30); // Position the button at the top right
        btnNotifications.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                	
                    NotificationsGUI notificationsGUI = new NotificationsGUI(connectedUser);
                    notificationsGUI.setVisible(true);
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
        });
        frame.getContentPane().add(btnNotifications);
 
        // Label for unread notifications count
        JLabel lblUnreadCount = new JLabel(String.valueOf(unreadNotifications));
        lblUnreadCount.setFont(new Font("Segoe UI", Font.BOLD, 12));
        lblUnreadCount.setForeground(Color.RED);
        lblUnreadCount.setBounds(630, 10, 20, 20); // Adjust position accordingly
        frame.getContentPane().add(lblUnreadCount);
        
        JButton btnGererInscriptions_1 = new JButton("Journalisation");
        btnGererInscriptions_1.setForeground(Color.WHITE);
        btnGererInscriptions_1.setFont(new Font("Segoe UI", Font.PLAIN, 16));
        btnGererInscriptions_1.setBackground(new Color(22, 100, 255));
        btnGererInscriptions_1.setBounds(240, 468, 300, 40);
        btnGererInscriptions_1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                
                new Actions();
            }
        });
        frame.getContentPane().add(btnGererInscriptions_1);
        
        JButton btnVoirHistorique_1 = new JButton("Visualiser les recherches");
        btnVoirHistorique_1.setForeground(Color.WHITE);
        btnVoirHistorique_1.setFont(new Font("Segoe UI", Font.PLAIN, 16));
        btnVoirHistorique_1.setBackground(new Color(22, 100, 255));
        btnVoirHistorique_1.setBounds(417, 399, 300, 40);
        btnVoirHistorique_1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	frame.dispose();
                new RechercheInterfaceGUI();
            }
        });
        frame.getContentPane().add(btnVoirHistorique_1);
        
    }
}