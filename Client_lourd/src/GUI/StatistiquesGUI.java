package GUI;

import DAO.HistoriqueDAO;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.labels.StandardPieSectionLabelGenerator;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;
import com.formdev.flatlaf.FlatLightLaf;

import javax.swing.*;
import java.awt.*;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

public class StatistiquesGUI extends JFrame {
    private HistoriqueDAO historiqueDAO;
    private JPanel barChartPanel;
    private JPanel pieChartPanel;
    private JSpinner startDateSpinner;
    private JSpinner endDateSpinner;
    private JButton updateButton;
    private JButton btn24h;
    private JButton btnLastWeek;
    private JButton btnLast3Months;
    private JButton btnAll;
    private JButton backButton;

    public StatistiquesGUI() throws SQLException {
        historiqueDAO = new HistoriqueDAO();
        initialize();
    }

    private void initialize() throws SQLException {
        // Set FlatLaf look and feel
        try {
            UIManager.setLookAndFeel(new FlatLightLaf());
        } catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }

        setTitle("Statistiques des Connexions");
        setBounds(100, 100, 1000, 700);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);
        getContentPane().setLayout(new BorderLayout());

        JPanel controlPanel = new JPanel();
        controlPanel.setLayout(new BorderLayout());

        JPanel datePanel = new JPanel(new FlowLayout());
        startDateSpinner = new JSpinner(new SpinnerDateModel());
        endDateSpinner = new JSpinner(new SpinnerDateModel());
        updateButton = new JButton("Mettre à jour");

        datePanel.add(new JLabel("Date de début:"));
        datePanel.add(startDateSpinner);
        datePanel.add(new JLabel("Date de fin:"));
        datePanel.add(endDateSpinner);
        datePanel.add(updateButton);

        btn24h = new JButton("24h");
        btnLastWeek = new JButton("Semaine dernière");
        btnLast3Months = new JButton("3 derniers mois");
        btnAll = new JButton("Tout");

        JPanel filterPanel = new JPanel(new FlowLayout());
        filterPanel.add(btn24h);
        filterPanel.add(btnLastWeek);
        filterPanel.add(btnLast3Months);
        filterPanel.add(btnAll);

        backButton = new JButton("Retour");
        backButton.setBackground(Color.RED);
        backButton.setForeground(Color.WHITE);

        controlPanel.add(backButton, BorderLayout.WEST);
        controlPanel.add(datePanel, BorderLayout.CENTER);
        controlPanel.add(filterPanel, BorderLayout.EAST);

        getContentPane().add(controlPanel, BorderLayout.NORTH);

        barChartPanel = new JPanel(new BorderLayout());
        pieChartPanel = new JPanel(new BorderLayout());

        JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, barChartPanel, pieChartPanel);
        splitPane.setResizeWeight(0.5);
        getContentPane().add(splitPane, BorderLayout.CENTER);

        updateButton.addActionListener(e -> {
            try {
                updateCharts();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        });

        btn24h.addActionListener(e -> setFilter("24h"));
        btnLastWeek.addActionListener(e -> setFilter("Semaine dernière"));
        btnLast3Months.addActionListener(e -> setFilter("3 derniers mois"));
        btnAll.addActionListener(e -> setFilter("Tout"));

        backButton.addActionListener(e -> goBack());

        // Initial default filter
        setFilter("24h");
    }

    private void setFilter(String filterType) {
        Calendar startCal = Calendar.getInstance();
        Calendar endCal = Calendar.getInstance();

        switch (filterType) {
            case "24h":
                startCal.add(Calendar.DAY_OF_MONTH, -1);
                break;
            case "Semaine dernière":
                startCal.add(Calendar.DAY_OF_MONTH, -7);
                break;
            case "3 derniers mois":
                startCal.add(Calendar.MONTH, -3);
                break;
            case "Tout":
                startCal.set(1970, Calendar.JANUARY, 1); // Arbitrary old date
                break;
        }

        startDateSpinner.setValue(startCal.getTime());
        endDateSpinner.setValue(endCal.getTime());

        try {
            updateCharts();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    private void updateCharts() throws SQLException {
        Date startDate = (Date) startDateSpinner.getValue();
        Date endDate = (Date) endDateSpinner.getValue();

        if (startDate == null || endDate == null) {
            JOptionPane.showMessageDialog(this, "Les dates de début et de fin doivent être sélectionnées.", "Erreur", JOptionPane.ERROR_MESSAGE);
            return;
        }

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String startDateString = sdf.format(startDate);
        String endDateString = sdf.format(endDate);

        Map<String, Integer> hourlyStats = historiqueDAO.getConnectionStatsBy4HourIntervals(startDateString, endDateString);
        Map<String, Integer> successFailureStats = historiqueDAO.getSuccessFailureStats(startDateString, endDateString);

        updateBarChart(barChartPanel, "Connexions par tranche de 4 heures", hourlyStats);
        updatePieChart(pieChartPanel, "Proportion des connexions réussies et échouées", successFailureStats);
    }

    private void updateBarChart(JPanel panel, String title, Map<String, Integer> data) {
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        // Define the 4-hour intervals
        String[] intervals = {"00:00-03:59", "04:00-07:59", "08:00-11:59", "12:00-15:59", "16:00-19:59", "20:00-23:59"};
        
        // Populate the dataset, ensuring all intervals are represented
        for (String interval : intervals) {
            dataset.addValue(data.getOrDefault(interval, 0), "Connexions", interval);
        }

        JFreeChart chart = ChartFactory.createBarChart(
                title,
                "Période",
                "Nombre de connexions",
                dataset
        );

        ChartPanel chartPanel = new ChartPanel(chart);
        panel.removeAll();
        panel.add(chartPanel, BorderLayout.CENTER);
        panel.validate();
    }

    private void updatePieChart(JPanel panel, String title, Map<String, Integer> data) {
        DefaultPieDataset dataset = new DefaultPieDataset();
        data.forEach(dataset::setValue);

        JFreeChart chart = ChartFactory.createPieChart(title, dataset, true, true, false);
        PiePlot plot = (PiePlot) chart.getPlot();
        plot.setLabelGenerator(new StandardPieSectionLabelGenerator("{0}: {1} ({2})"));
        plot.setSectionPaint("succès", new Color(0, 204, 0)); // Green for success
        plot.setSectionPaint("echec", new Color(255, 0, 0)); // Red for failure

        ChartPanel chartPanel = new ChartPanel(chart);
        panel.removeAll();
        panel.add(chartPanel, BorderLayout.CENTER);
        panel.validate();
    }

    private void goBack() {
        this.dispose();
        EventQueue.invokeLater(() -> {
            try {
                HistoriqueInterfaceGUI frame = new HistoriqueInterfaceGUI(null);
                frame.setVisible(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            try {
                StatistiquesGUI frame = new StatistiquesGUI();
                frame.setVisible(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }
}
