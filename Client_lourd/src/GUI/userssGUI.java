package GUI;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import DAO.*;
import MODEL.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;
import com.formdev.flatlaf.FlatLightLaf;
import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;

public class userssGUI {
	 
    private JFrame frame;
    private JTable table;
    private JComboBox<String> roleComboBox;
    private DefaultTableModel model;
 
    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            try {
                // Intégration de FlatLaf
                FlatLightLaf.install();
                UIManager.setLookAndFeel(new FlatLightLaf());
 
                userssGUI window = new userssGUI(null);
                window.frame.setVisible(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }
 
    public userssGUI(User connectedUser) {
        initialize(connectedUser);
        this.frame.setVisible(true);
    }

    private void initialize(User connectedUser) {
        frame = new JFrame();
        frame.setTitle("Gestion des utilisateurs");
        frame.setBounds(100, 100, 800, 600);
        //frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
       

        JPanel controlPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        controlPanel.setBackground(Color.gray);
        frame.getContentPane().add(controlPanel, BorderLayout.NORTH);

        roleComboBox = new JComboBox<>();
        roleComboBox.addItem("Tous les utilisateurs");
        roleComboBox.addItem("Admin");
        roleComboBox.addItem("Entraineur");
        roleComboBox.addItem("Président");
        roleComboBox.addItem("Elu");
        controlPanel.add(roleComboBox);

    // écouteur d'événements sur le JComboBox
        roleComboBox.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                
                filterUsersByRole((String) roleComboBox.getSelectedItem());
            }
        });


        JScrollPane scrollPane = new JScrollPane();
        frame.getContentPane().add(scrollPane, BorderLayout.CENTER);

        model = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return column == 5 || column == 6;
            }
        };

     // modèle de table 
        model = new DefaultTableModel();
        model.addColumn("Nom");
        model.addColumn("Prénom");
        model.addColumn("Email");
        model.addColumn("Etablissement");
        model.addColumn("Type Utilisateur");
        model.addColumn("Supprimer");
        model.addColumn("Réinitialisation du mot de passe");


        table = new JTable(model);
        scrollPane.setViewportView(table);
        
       
     // Utilise un objet "ButtonRenderer" pour personnaliser le rendu de la cellule de la colonne d'indice 5 et 6
        table.getColumnModel().getColumn(5).setCellRenderer(new ButtonRenderer(Color.RED));
        table.getColumnModel().getColumn(6).setCellRenderer(new ButtonRenderer(Color.BLUE));
 
        // Utilise un objet ButtonEditor pour définir le comportement des colonnes "Supprimer" et "Réinitialisation du mot de passe"
        table.getColumnModel().getColumn(5).setCellEditor(new ButtonEditor(new JCheckBox(), Color.RED, "Supprimer",connectedUser));
        table.getColumnModel().getColumn(6).setCellEditor(new ButtonEditor(new JCheckBox(), Color.BLUE, "Supprimer",connectedUser));
     

           // Panel pour le bouton retour
                JPanel bottomPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 10, 10));
                frame.getContentPane().add(bottomPanel, BorderLayout.SOUTH);
         
                // Bouton retour
                JButton btnRetour = new JButton("Retour");
                btnRetour.setFont(new Font("Segoe UI", Font.PLAIN, 14));
                btnRetour.setBackground(new Color(255, 69, 58)); // Couleur rouge pour le bouton retour
                btnRetour.setForeground(Color.WHITE);
                btnRetour.setFocusPainted(false);
                btnRetour.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        frame.dispose();
                        // Retourner à l'interface admin
                        try {
        					new AdminGUI(connectedUser);
        				} catch (SQLException e1) {
        					// TODO Auto-generated catch block
        					e1.printStackTrace();
        				}
                    }
                });
                bottomPanel.add(btnRetour);

        loadAllUsers();
        
    }
    

    private void loadAdmins() {
        model.setRowCount(0);
        try {
            UserDAO userDAO = new UserDAO();
            ArrayList<User> users = userDAO.getUser();
            for (User user : users) {
                addRowToTableAdmin(user);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
    private void loadAllUsers() {
        model.setRowCount(0);
        try {
            UserDAO userDAO = new UserDAO();
            ArrayList<User> users = userDAO.getUser();
            for (User user : users) {
                addRowToTableAdmin(user);
            }
            inscriptionDAO ins = new inscriptionDAO();
            ArrayList<inscription> userss = ins.getUsers();
            for (inscription use : userss) {
                addRowToTable(use);
            }
            
            
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    private void filterUsersByRole(String role) {
        try {
            model.setRowCount(0);
            if (role.equals("Tous les utilisateurs")) {
                loadAllUsers();
            } 
            if (role.equals("Admin")) {
                loadAdmins();
            } 
            else {
                inscriptionDAO u = new inscriptionDAO();
                ArrayList<inscription> i = u.getUserbyRole(role);
                for (inscription ins : i) {
                    addRowToTable(ins);
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    private void addRowToTableAdmin(User user) {
        model.addRow(new Object[]{user.getNom(), user.getPrenom(), user.getEmail(), user.getCommune(), user.getUserType(), "Supprimer", "Réinitialiser"});
    }
    private void addRowToTable(inscription ins) {
        model.addRow(new Object[]{ins.getNom(), ins.getPrenom(), ins.getEmail(), ins.getEtablissement(), ins.getType(),  "Supprimer", "Réinitialiser"});
    }

    
//rendu visuel bouton
    class ButtonRenderer extends DefaultTableCellRenderer {
    	private final JButton button;
        private final String buttonText;

        public ButtonRenderer(Color color) {
           button = new JButton();
		this.buttonText = "";
            button.setOpaque(true);
            button.setBackground(color);
        }

        public Component getTableCellRendererComponent(JTable table, Object value,
                                                       boolean isSelected, boolean hasFocus,
                                                       int row, int column) {
            return button;
        }
    }
    
    // Comportement
    class ButtonEditor extends DefaultCellEditor {
        private final JButton button;
        private String label;
        private final String buttonText;

        public ButtonEditor(JCheckBox checkBox, Color color, String text,User connectedUser) {
            super(checkBox);
            buttonText = text;
            button = new JButton();
            button.setBackground(color);
            button.setForeground(Color.WHITE);
            button.addActionListener(e -> {
            	int row = table.convertRowIndexToModel(table.getEditingRow());
                String email = (String) table.getModel().getValueAt(row, 2);
                String role = (String) table.getModel().getValueAt(row, 4);
                if (button.getText().equals("Supprimer")) {
                    int option = JOptionPane.showConfirmDialog(frame, "Voulez-vous vraiment supprimer cet utilisateur ?", "Confirmation de suppression", JOptionPane.YES_NO_OPTION);
                    if (option == JOptionPane.YES_OPTION) {
                        DeleteUser(connectedUser,row, email, role);
                    }
                } else if (button.getText().equals("Réinitialiser")) {
                    int option = JOptionPane.showConfirmDialog(frame, "Voulez-vous vraiment réinitialiser le mot de passe de cet utilisateur ?", "Confirmation de réinitialisation du mot de passe", JOptionPane.YES_NO_OPTION);
                    if (option == JOptionPane.YES_OPTION) {
                        ResetPassword(connectedUser,email, role);
                    }
                }
                button.setEnabled(true);
            });
        }
        private void DeleteUser(User connectedUser,int row, String email, String role) {
            try {
                if (role.equals("Admin")) {
                    UserDAO userDAO = new UserDAO();
                    int result = userDAO.deleteUser(email);
                    int id = userDAO.getId(email);
                    if (result == 1) {
                        model.removeRow(row);
                      //AUDIT ACTION
                        actionDAO a = new actionDAO();
						a.insertAction(connectedUser.getId(),"Suppression d'un Administrateur", email);
                    }
                }
                    else {
                        inscriptionDAO ins = new inscriptionDAO();
                        int result1 = ins.delete(email);
                        int id = ins.getId(email);
                        if (result1 == 1) {
                            model.removeRow(row);
                          //AUDIT ACTION
                            actionDAO a = new actionDAO();
                            a.insertAction(connectedUser.getId(),"Suppression d'un utilisateur", email);
                            
                        }
                    }
                
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(frame, "Erreur lors de la suppression de l'utilisateur : " + ex.getMessage(), "Erreur", JOptionPane.ERROR_MESSAGE);
            }
        }

        private void ResetPassword(User connectedUser,String email, String role) {
            try {
                if (role.equals("admin")) {
                	UserDAO userDAO = new UserDAO();
                    int result = userDAO.reinitialiser(email);
                    if (result == 1) {
                        //SEND EMAIL;
                        String subject = "Réinitialisation mot de passe ";
                        String body = "Bonjour,\n\n Veuillez réinitialiser le mot de passe de votre compte \n\nMerci.";
                        sendEmail(email, subject, body);
                        
                        //AUDIT ACTION
                        actionDAO a = new actionDAO();
                        
						a.insertAction(connectedUser.getId(),"Reinitialisation mot de passe admin", email);
                        
                    }
                    
                }
                else {
                	inscriptionDAO ins = new inscriptionDAO();
                    int result1 = ins.reinitialiser(email);
                    if (result1 == 1) {
                        //SEND EMAIL
                    	String subject = "Réinitialisation mot de passe ";
                        String body = "Bonjour,\n\n Veuillez réinitialiser le mot de passe de votre compte \n\nMerci.";
                        sendEmail(email, subject, body);
                      //AUDIT ACTION
                        actionDAO a = new actionDAO();

						a.insertAction(connectedUser.getId(),"Reinitialisation mot de passe utilisateurs", email);
                        
                    }
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(frame, "Erreur lors de la réinitialisation du mot de passe : " + ex.getMessage(), "Erreur", JOptionPane.ERROR_MESSAGE);
            }
        }
        
        
        
        
        
        //personnaliser l'apparence de la cellule 

        public Component getTableCellEditorComponent(JTable table, Object value,
                                                     boolean isSelected, int row, int column) {
            if (isSelected) {
                button.setForeground(table.getSelectionForeground());
                button.setBackground(table.getSelectionBackground());
            } else {
                button.setForeground(table.getForeground());
                button.setBackground(UIManager.getColor("Button.background"));
            }
            label = (value == null) ? "" : value.toString();
           button.setText(label);
            return button;
        }
    }
    
    public void sendEmail(String recipientEmail, String subject, String body) {
        final String username = "clubssportif@gmail.com";
        final String password = "kdsabinqkpvkruhm"; // Remplacez par votre mot de passe d'application si l'authentification en deux étapes est activée

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
        
        Session session = Session.getInstance(props,
            new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(username, password);
                }
            });

        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(username));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipientEmail));
            message.setSubject(subject);
            message.setText(body);

            Transport.send(message);
            System.out.println("Email envoyé avec succès!");

        } catch (MessagingException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
    
}
