package GUI;

import com.formdev.flatlaf.FlatLightLaf;
import org.mindrot.jbcrypt.BCrypt;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

import javax.imageio.ImageIO;
import javax.swing.*;

import DAO.UserDAO;
import MODEL.User;

public class ConnexionAdminGUI {

    private JFrame frame;
    private JPasswordField passwordField;
    private JTextField textField;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    FlatLightLaf.install();
                    UIManager.setLookAndFeel(new FlatLightLaf());
                    ConnexionAdminGUI window = new ConnexionAdminGUI();
                    window.frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the application.
     */
    public ConnexionAdminGUI() {
        initialize();
        this.frame.setVisible(true);
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        UserDAO user = new UserDAO();
        frame = new JFrame("Plateforme Administrateur - Club Sportif");
        frame.setBounds(100, 100, 800, 600);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.getContentPane().setLayout(null);

        JLabel lblErreur = new JLabel("");
        lblErreur.setForeground(Color.RED);
        lblErreur.setFont(new Font("Segoe UI", Font.PLAIN, 14));
        lblErreur.setBounds(300, 50, 200, 20);
        frame.getContentPane().add(lblErreur);

        JLabel lblIdentifiant = new JLabel("Identifiant");
        lblIdentifiant.setFont(new Font("Segoe UI", Font.PLAIN, 18));
        lblIdentifiant.setForeground(Color.BLACK);
        lblIdentifiant.setBounds(200, 150, 100, 25);
        frame.getContentPane().add(lblIdentifiant);

        textField = new JTextField();
        textField.setFont(new Font("Segoe UI", Font.PLAIN, 16));
        textField.setBounds(350, 150, 200, 30);
        frame.getContentPane().add(textField);
        textField.setColumns(10);

        JLabel lblMotDePasse = new JLabel("Mot de passe");
        lblMotDePasse.setFont(new Font("Segoe UI", Font.PLAIN, 18));
        lblMotDePasse.setForeground(Color.BLACK);
        lblMotDePasse.setBounds(200, 220, 120, 25);
        frame.getContentPane().add(lblMotDePasse);

        passwordField = new JPasswordField();
        passwordField.setFont(new Font("Segoe UI", Font.PLAIN, 16));
        passwordField.setBounds(350, 220, 200, 30);
        frame.getContentPane().add(passwordField);
        
        JCheckBox showPasswordCheckBox = new JCheckBox("Afficher le mot de passe");
        showPasswordCheckBox.setFont(new Font("Segoe UI", Font.PLAIN, 14));
        showPasswordCheckBox.setBounds(350, 260, 200, 30);
        frame.getContentPane().add(showPasswordCheckBox);

        showPasswordCheckBox.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (showPasswordCheckBox.isSelected()) {
                    passwordField.setEchoChar((char) 0); // Affiche le mot de passe
                } else {
                    passwordField.setEchoChar('\u2022'); // Masque le mot de passe
                }
            }
        });

        JButton btnConnexion = new JButton("Connexion");
        btnConnexion.setFont(new Font("Segoe UI", Font.BOLD, 16));
        btnConnexion.setForeground(Color.WHITE);
        btnConnexion.setBackground(new Color(22, 100, 255));
        btnConnexion.setBounds(350, 300, 150, 40);
        btnConnexion.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String identifiant = textField.getText();
                String pass = new String(passwordField.getPassword());
                String passwd = user.getMdp(identifiant);
                if (passwd != null && BCrypt.checkpw(pass, passwd)) {
                    User connectedUser = user.userConnection(identifiant, passwd);
                    if (connectedUser != null && "Admin".equals(connectedUser.getUserType())) {
                        JOptionPane.showMessageDialog(frame, "Connexion réussie! Bienvenue, Admin.", "Confirmation", JOptionPane.INFORMATION_MESSAGE);
                        try {
                            new AdminGUI(connectedUser);
                        } catch (SQLException e1) {
                            e1.printStackTrace();
                        }
                        frame.setVisible(false);
                    } 
                } else {
                    JOptionPane.showMessageDialog(frame, "Erreur login ou password", "Erreur", JOptionPane.ERROR_MESSAGE);
                    textField.setText("");
                    passwordField.setText("");
                }
            }
        });
        frame.getContentPane().add(btnConnexion);

        JLabel lblTitle = new JLabel("Club Sportif - Admin");
        lblTitle.setFont(new Font("Segoe UI", Font.BOLD, 24));
        lblTitle.setBounds(250, 50, 300, 40);
        lblTitle.setForeground(new Color(22, 100, 255));
        frame.getContentPane().add(lblTitle);

    }
    }

