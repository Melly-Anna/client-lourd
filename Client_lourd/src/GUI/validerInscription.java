package GUI;

import java.awt.Desktop;

import java.awt.EventQueue;
import org.mindrot.jbcrypt.BCrypt;

import DAO.actionDAO;
import MODEL.User;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableModel;
 
import org.mindrot.jbcrypt.BCrypt;
 
import com.formdev.flatlaf.FlatLightLaf;
 
import MODEL.User;

public class validerInscription {
	 
    private JFrame frame;
    private JTable table;
 
    private static final String URL = "jdbc:mysql://localhost:3306/club_de_sport";
    private static final String USER = "root";
    private static final String PASSWORD = "root";
 
    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            try {
                FlatLightLaf.install();
                UIManager.setLookAndFeel(new FlatLightLaf());
                validerInscription window = new validerInscription(null);
                window.frame.setVisible(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }
 
    public validerInscription(User connectedUser) {
        initialize(connectedUser);
        loadInscriptions();
        this.frame.setVisible(true);
    }

 

    /**
     * Initialize the contents of the frame.
     */
    private void initialize(User connectedUser) {
        frame = new JFrame();
        frame.setBounds(100, 100, 600, 400);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setTitle("DEMANDES D'INSCRIPTIONS");
        frame.getContentPane().setLayout(new BorderLayout(10, 10));
 
        JScrollPane scrollPane = new JScrollPane();
        frame.getContentPane().add(scrollPane, BorderLayout.CENTER);
 
        table = new JTable();
        scrollPane.setViewportView(table);
 
        JPanel buttonPanel = new JPanel();
        frame.getContentPane().add(buttonPanel, BorderLayout.SOUTH);
 
        JButton btnValider = new JButton("Valider");
        btnValider.setBackground(new Color(34, 139, 34)); // Couleur verte pour valider
        btnValider.setForeground(Color.WHITE);
        btnValider.addActionListener(e -> updateValidation(connectedUser));
        buttonPanel.add(btnValider);
 
        JButton btnRefuser = new JButton("Refuser");
        btnRefuser.setBackground(new Color(255, 69, 58)); // Couleur rouge pour refuser
        btnRefuser.setForeground(Color.WHITE);
        btnRefuser.addActionListener(e -> removeInscription(connectedUser));
        buttonPanel.add(btnRefuser);
 
        JButton btnVisualiser = new JButton("Visualiser le justificatif");
        btnVisualiser.setBackground(new Color(30, 144, 255)); // Couleur bleue pour visualiser
        btnVisualiser.setForeground(Color.WHITE);
        btnVisualiser.addActionListener(e -> justificatif());
        buttonPanel.add(btnVisualiser);
 
        JButton btnRetour = new JButton("Retour");
        btnRetour.setBackground(Color.GRAY); // Couleur grise pour retour
        btnRetour.setForeground(Color.WHITE);
        btnRetour.addActionListener(e -> {
            frame.dispose();
            try {
				new AdminGUI(connectedUser);
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
        });
        buttonPanel.add(btnRetour);
    }


 // Méthode pour charger les inscriptions depuis la base de données
    private void loadInscriptions() {
        try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);
             PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM Inscription WHERE Valider = ?")) {
            
            preparedStatement.setInt(1, 0); 
            
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                // Créer un modèle de table
                DefaultTableModel model = new DefaultTableModel();
                table.setModel(model);

                // Ajouter les colonnes au modèle de table
                model.addColumn("Inscription_ID");
                model.addColumn("Nom");
                model.addColumn("Prenom");
                model.addColumn("Email");
                model.addColumn("Type");
                
                // Ajouter les données de la base de données au modèle de table
                while (resultSet.next()) {
                    Object[] row = {
                        resultSet.getInt("Inscription_ID"),
                        resultSet.getString("Nom"),
                        resultSet.getString("Prenom"),
                        resultSet.getString("Email"),
                        resultSet.getString("Type")
                    };
                    model.addRow(row);
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }



    // Méthode pour mettre à jour la validation d'une inscription
    private void updateValidation(User connectedUser) {
        // Récupérer l'index de la ligne sélectionnée
        int rowIndex = table.getSelectedRow();
        if (rowIndex != -1) {
            try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
                // Mettre à jour la colonne "Valider" avec 1

                int inscriptionId = (int) table.getValueAt(rowIndex, 0);
                String email = (String) table.getValueAt(rowIndex, 3); 
                String updateQuery = "UPDATE Inscription SET Valider = 1 WHERE Inscription_ID = ?";
                try (PreparedStatement preparedStatement = connection.prepareStatement(updateQuery)) {
                    preparedStatement.setInt(1, inscriptionId);
                    preparedStatement.executeUpdate();
                }

                // Demander le mot de passe
                String password = JOptionPane.showInputDialog(frame, "Veuillez entrer le mot de passe :");
                

             // Crypter le mot de passe avec Bcrypt
             String hashedPassword = BCrypt.hashpw(password, BCrypt.gensalt());
                

                // Mettre à jour la colonne "Password" avec le mot de passe
                String passwordUpdateQuery = "UPDATE Inscription SET Password = ? WHERE Inscription_ID = ?";
                try (PreparedStatement preparedStatement = connection.prepareStatement(passwordUpdateQuery)) {
                    preparedStatement.setString(1, hashedPassword);
                    preparedStatement.setInt(2, inscriptionId);
                    preparedStatement.executeUpdate();
                }

             // Envoyer l'email à l'utilisateur
                String subject = "Votre compte a été validé";
                String body = "Bonjour,\n\nVotre compte a été validé. Votre mot de passe est : " + password + "\n\nMerci.";
                sendEmail(email, subject, body);

                // Recharger les inscriptions après la mise à jour

                loadInscriptions();
                
                //audit action 
                actionDAO a = new actionDAO();
                a.insertAction(connectedUser.getId(), "Validation inscription", email);
                
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        } else {

            JOptionPane.showMessageDialog(frame, "Veuillez sélectionner une inscription à valider.");
        }
    }

    // Méthode pour supprimer une inscription
    private void removeInscription(User connectedUser) {
      
        int rowIndex = table.getSelectedRow();
        if (rowIndex != -1) {
            try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
              

                int inscriptionId = (int) table.getValueAt(rowIndex, 0);
                String email = (String) table.getValueAt(rowIndex, 3); 
                String deleteQuery = "DELETE FROM Inscription WHERE Inscription_ID = ?";
                try (PreparedStatement preparedStatement = connection.prepareStatement(deleteQuery)) {
                    preparedStatement.setInt(1, inscriptionId);
                    preparedStatement.executeUpdate();
                }

                // Recharger les inscriptions après la suppression

                loadInscriptions();
                //
                actionDAO a = new actionDAO();
                a.insertAction(connectedUser.getId(), "Refus inscription", email);
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        } else {

            JOptionPane.showMessageDialog(frame, "Veuillez sélectionner une inscription à refuser.");

        }
    }
    

    private void justificatif() {
       
        int rowIndex = table.getSelectedRow();
        if (rowIndex != -1) {
            try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
                // Récupérer l'identifiant de l'inscription sélectionnée
                int inscriptionId = (int) table.getValueAt(rowIndex, 0);

                // Préparer la requête SQL pour récupérer la preuve de l'inscription
                String sql = "SELECT preuve FROM Inscription WHERE Inscription_ID = ?";
                try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
                    preparedStatement.setInt(1, inscriptionId);

                    try (ResultSet resultSet = preparedStatement.executeQuery()) {
                        if (resultSet.next()) {
                            // Récupérer les données BLOB
                            Blob blob = resultSet.getBlob("preuve");
                            if (blob != null) {
                                // Récupérer les données sous forme de tableau de bytes
                                byte[] bytes = blob.getBytes(1, (int) blob.length());

                                // Demander à l'utilisateur où enregistrer le fichier
                                JFileChooser fileChooser = new JFileChooser();
                                fileChooser.setDialogTitle("Enregistrer le justificatif");
                                int userSelection = fileChooser.showSaveDialog(frame);
                                if (userSelection == JFileChooser.APPROVE_OPTION) {
                                    // Récupérer le chemin du fichier sélectionné
                                    String fileName = fileChooser.getSelectedFile().getAbsolutePath();

                                    // Enregistrer les données dans le fichier sélectionné
                                    try (FileOutputStream fos = new FileOutputStream(fileName)) {
                                        fos.write(bytes);
                                        fos.flush();
                                        JOptionPane.showMessageDialog(frame, "Le justificatif a été téléchargé avec succès !");
                                        
                                        // Ouvrir le fichier
                                        Desktop.getDesktop().open(new File(fileName));
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                        JOptionPane.showMessageDialog(frame, "Erreur lors de l'écriture du fichier !");
                                    }
                                }
                            } else {
                                JOptionPane.showMessageDialog(frame, "Aucun justificatif trouvé pour cette inscription !");
                            }
                        }
                    }
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
                JOptionPane.showMessageDialog(frame, "Erreur lors de la récupération du justificatif !");
            }
        } else {
            JOptionPane.showMessageDialog(frame, "Veuillez sélectionner une inscription pour télécharger le justificatif !");
        }
    }

    public void sendEmail(String recipientEmail, String subject, String body) {
        final String username = "clubssportif@gmail.com";
        final String password = "kdsabinqkpvkruhm"; // Remplacez par votre mot de passe d'application si l'authentification en deux étapes est activée

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
        
        Session session = Session.getInstance(props,
            new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(username, password);
                }
            });

        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(username));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipientEmail));
            message.setSubject(subject);
            message.setText(body);

            Transport.send(message);
            System.out.println("Email envoyé avec succès!");

        } catch (MessagingException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }


}