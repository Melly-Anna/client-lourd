package DAO;
import java.util.ArrayList;
import java.sql.*;
import MODEL.*;



public class UserDAO extends ConnectionDAO  {

	/**
	 * Constructor
	 * 
	 */
	public UserDAO() {
		super();
	}
	
	/**
	 * Récupère la liste de tous les utilisateurs dans la base de données.
	 * 
	 * @return une liste d'objets d'utilisateurs 
	 * @throws SQLException si une erreur survient lors de la récupération des utilisateurs depuis la base de données
	 */
	public ArrayList<User> getUser() throws SQLException {
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		ArrayList<User> users = new ArrayList<>();
		try {
			// Connect to the database
			con = DriverManager.getConnection(URL, LOGIN, PASS);

			String sql = "SELECT * FROM users";
			stmt = con.prepareStatement(sql);
			// Execute the query
			rs = stmt.executeQuery();

			// Process the results
			while (rs.next()) {
				// Create Etablissement objects and add them to the list
				User u = new User();
				u.setId(rs.getInt("id"));
				u.setNom(rs.getString("nom"));
				u.setPrenom(rs.getString("prenom"));
				u.setPassword(rs.getString("password"));
				u.setEmail(rs.getString("email"));
				u.setUserType(rs.getString("userType"));
				u.setCommune(rs.getInt("commune"));
				u.setFederation(rs.getString("federation"));
				users.add(u);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// fermeture du rs, du preparedStatement et de la connexion
						try {
							if (rs != null)
								rs.close();
						} catch (Exception ignore) {
						}
						try {
							if (stmt!= null)
								stmt.close();
						} catch (Exception ignore) {
						}
						try {
							if (con != null)
								con.close();
						} catch (Exception ignore) {
						}
					}
		return users;
	}
		
		
	/**
	 * Récupère un utilisateur selon son email et son password
	 * 
	 * @return un utilisateur
	 * @throws SQLException si une erreur survient lors de la récupération des utilisateurs depuis la base de données
	 */    
	public User userConnection(String email, String password) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		User returnValue = null;

		// connexion a la base de donnees
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM users WHERE email = ? AND password = ?");
			ps.setString(1, email);
			ps.setString(2, password);;

			// on execute la requete
			// rs contient un pointeur situe juste avant la premiere ligne retournee
			rs = ps.executeQuery();
			// passe a la premiere (et unique) ligne retournee
			if (rs.next()) {
				returnValue = new User( rs.getInt("Id"),
						rs.getString("nom"),
					    rs.getString("prenom"),
					    rs.getString("password"),
					    rs.getString("email"),
					    rs.getString("userType"), 
					    rs.getInt("commune"),
					    rs.getString("federation"));
			}
		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du ResultSet, du PreparedStatement et de la Connexion
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}
	/**
	 * Permet de supprimer un utilisateur par email dans la table users.
	 * Le mode est auto-commit par defaut : chaque suppression est validee
	 * @param email l'adresse mail de l'user à supprimer
	 * @return retourne le nombre de lignes supprimees dans la table
	 */
	public int deleteUser(String email) {
		Connection con = null;
		PreparedStatement ps = null;
		int returnValue = 0;

		// connexion a la base de donnees
		try {

			// tentative de connexion
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			// preparation de l'instruction SQL, le ? represente la valeur de l'ID
			// a communiquer dans la suppression.
			// le getter permet de recuperer la valeur de l'ID de l'user
			ps = con.prepareStatement("DELETE FROM users WHERE email = ?");
			ps.setString(1, email);

			// Execution de la requete
			returnValue = ps.executeUpdate();

		} catch (Exception e) {
			
				e.printStackTrace();
		} finally {
			// fermeture du preparedStatement et de la connexion
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}
	
	public int reinitialiser(String email) {
		Connection con = null;
		PreparedStatement ps = null;
		int returnValue = 0;

		// connexion a la base de donnees
		try {

			// tentative de connexion
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			// preparation de l'instruction SQL, le ? represente la valeur de l'ID
			// a communiquer dans la suppression.
			// le getter permet de recuperer la valeur de l'ID de l'user
			ps = con.prepareStatement("UPDATE users set reinitialiser = ? WHERE email= ?");
			ps.setInt(1, 1);
			ps.setString(2, email);

			// Execution de la requete
			returnValue = ps.executeUpdate();

		} catch (Exception e) {
			
				e.printStackTrace();
		} finally {
			// fermeture du preparedStatement et de la connexion
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}
	
	
	
	
	
	
	/**
	 * Permet d'ajouter un utilisateur 
	 * Le mode est auto-commit par defaut : chaque ajout est validee
	 * @param l'user à rajouter
	 * @return retourne le nombre de lignes ajoutées dans la table
	 */
	public int add(User user) {
	    Connection con = null;
	    PreparedStatement ps = null;
	    int returnValue = 0;

	    // Connexion à la base de données
	    try {
	        // Tentative de connexion
	        con = DriverManager.getConnection(URL, LOGIN, PASS);
	        // Préparation de l'instruction SQL, chaque ? représente une valeur à communiquer dans l'insertion.
	        ps = con.prepareStatement("INSERT INTO users(nom, prenom, email, password, userType,commune,federation) VALUES (?, ?, ?, ?, ?,?,?)");
	        ps.setString(1, user.getNom());
	        ps.setString(2, user.getPrenom());
	        ps.setString(3, user.getEmail());
	        ps.setString(4, user.getPassword());
	        ps.setString(5, user.getUserType());
	        ps.setInt(6, user.getCommune());
	        ps.setString(7, user.getFederation());


	        // Exécution de la requête
	        returnValue = ps.executeUpdate();

	    } catch (SQLException e) {
	        if (e.getMessage().contains("ORA-00001"))
	            System.out.println("Cet utilisateur existe déjà. Ajout impossible !");
	        else
	            e.printStackTrace();
	    } finally {
	        // Fermeture du preparedStatement et de la connexion
	        try {
	            if (ps != null) {
	                ps.close();
	            }
	        } catch (SQLException ignore) {
	        }
	        try {
	            if (con != null) {
	                con.close();
	            }
	        } catch (SQLException ignore) {
	        }
	    }
	    return returnValue;
	}

	
	/**
	 * Récupère un mot de passe utilisateur selon son email 
	 * 
	 * @return String
	 * @throws SQLException si une erreur survient lors de la récupération des utilisateurs depuis la base de données
	 */    
	public String getMdp(String email) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String returnValue = null;

		// connexion a la base de donnees
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT password FROM users WHERE email = ?");
			ps.setString(1, email);
			

			// on execute la requete
			// rs contient un pointeur situe juste avant la premiere ligne retournee
			rs = ps.executeQuery();
			// passe a la premiere (et unique) ligne retournee
			if (rs.next()) {
				returnValue =  rs.getString("password");
			}
		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du ResultSet, du PreparedStatement et de la Connexion
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}

	/**
	 * Récupère la liste de tous les utilisateurs dans la base de données.
	 * 
	 * @return une liste d'objets d'utilisateurs 
	 * @throws SQLException si une erreur survient lors de la récupération des utilisateurs depuis la base de données
	 */
	public ArrayList<User> getUserbyRole(String role) throws SQLException {
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		ArrayList<User> users = new ArrayList<>();
		try {
			// Connect to the database
			con = DriverManager.getConnection(URL, LOGIN, PASS);

			String sql = "SELECT * FROM users WHERE userType = ?";
			stmt = con.prepareStatement(sql);
			stmt.setString(1, role);
			// Execute the query
			rs = stmt.executeQuery();

			// Process the results
			while (rs.next()) {
				// Create Etablissement objects and add them to the list
				User u = new User();
				u.setId(rs.getInt("id"));
				u.setNom(rs.getString("nom"));
				u.setPrenom(rs.getString("prenom"));
				u.setPassword(rs.getString("password"));
				u.setEmail(rs.getString("email"));
				u.setUserType(rs.getString("userType"));
				u.setCommune(rs.getInt("commune"));
				u.setFederation(rs.getString("federation"));
				users.add(u);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// fermeture du rs, du preparedStatement et de la connexion
						try {
							if (rs != null)
								rs.close();
						} catch (Exception ignore) {
						}
						try {
							if (stmt!= null)
								stmt.close();
						} catch (Exception ignore) {
						}
						try {
							if (con != null)
								con.close();
						} catch (Exception ignore) {
						}
					}
		return users;
	}
	public int getId(String email) {
		Connection con = null;
		PreparedStatement ps = null;
		int returnValue=0;
		ResultSet rs = null;

		// connexion a la base de donnees
		try {

			// tentative de connexion
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			// preparation de l'instruction SQL, le ? represente la valeur de l'ID
			// a communiquer dans la suppression.
			// le getter permet de recuperer la valeur de l'ID de l'user
			ps = con.prepareStatement("SELECT id FROM users  WHERE email= ?");
			ps.setString(1, email);

			// on execute la requete
			// rs contient un pointeur situe juste avant la premiere ligne retournee
			rs = ps.executeQuery();
			// passe a la premiere (et unique) ligne retournee
			if (rs.next()) {
				returnValue =  rs.getInt("id");
			}
		}
			catch (Exception ee) {
				ee.printStackTrace();
			} finally {
			// fermeture du preparedStatement et de la connexion
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}
}
