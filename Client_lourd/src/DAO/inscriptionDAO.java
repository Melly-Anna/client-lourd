package DAO;
import java.util.ArrayList;

import com.mysql.cj.protocol.Resultset;

import java.sql.*;
import MODEL.*;

public class inscriptionDAO extends ConnectionDAO  {

	/**
	 * Constructor
	 * 
	 */
	public inscriptionDAO() {
		super();
	}
	
	/**
	 * Récupère la liste de toutes les inscriptions qui sont deja validées.
	 * 
	 * @return une liste d'objets d'inscription 
	 * @throws SQLException si une erreur survient lors de la récupération des utilisateurs depuis la base de données
	 */
	public ArrayList<inscription> getUsers() throws SQLException {
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		ArrayList<inscription> users = new ArrayList<>();
		try {
			// Connect to the database
			con = DriverManager.getConnection(URL, LOGIN, PASS);

			String sql = "SELECT * FROM inscription WHERE Valider = ? OR Valider = ?";
			stmt = con.prepareStatement(sql);
			stmt.setInt(1, 1);
			stmt.setInt(2, 2);
			
			// Execute the query
			rs = stmt.executeQuery();

			// Process the results
			while (rs.next()) {
				
				inscription u = new inscription();
				u.setId(rs.getInt("Inscription_ID"));
				u.setNom(rs.getString("Nom"));
				u.setPrenom(rs.getString("Prenom"));
				u.setEmail(rs.getString("Email"));
				u.setType(rs.getString("Type"));
				u.setEtablissement(rs.getInt("Etablissement_ID"));
				u.setPreuve(rs.getString("Preuve"));
				u.setValider(rs.getInt("Valider"));
				u.setPassword(rs.getString("password"));
				
				users.add(u);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// fermeture du rs, du preparedStatement et de la connexion
						try {
							if (rs != null)
								rs.close();
						} catch (Exception ignore) {
						}
						try {
							if (stmt!= null)
								stmt.close();
						} catch (Exception ignore) {
						}
						try {
							if (con != null)
								con.close();
						} catch (Exception ignore) {
						}
					}
		return users;
	}
	
	/**
	 * Récupère la liste de tous les utilisateurs validé selon le type.
	 * 
	 * @return une liste d'objets d'inscription 
	 * @throws SQLException si une erreur survient lors de la récupération des utilisateurs depuis la base de données
	 */
	public ArrayList<inscription> getUserbyRole(String role) throws SQLException {
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		ArrayList<inscription> users = new ArrayList<>();
		try {
			// Connect to the database
			con = DriverManager.getConnection(URL, LOGIN, PASS);

			String sql = "SELECT * FROM inscription WHERE Type = ? AND Valider = ?";
			stmt = con.prepareStatement(sql);
			stmt.setString(1, role);
			stmt.setInt(2, 1);
			// Execute the query
			rs = stmt.executeQuery();
			

			// Process the results
			while (rs.next()) {
				
				inscription u = new inscription();
				u.setId(rs.getInt("Inscription_ID"));
				u.setNom(rs.getString("Nom"));
				u.setPrenom(rs.getString("Prenom"));
				u.setEmail(rs.getString("Email"));
				u.setType(rs.getString("Type"));
				u.setEtablissement(rs.getInt("Etablissement_ID"));
				u.setPreuve(rs.getString("Preuve"));
				u.setValider(rs.getInt("Valider"));
				u.setPassword(rs.getString("password"));
				
				users.add(u);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// fermeture du rs, du preparedStatement et de la connexion
						try {
							if (rs != null)
								rs.close();
						} catch (Exception ignore) {
						}
						try {
							if (stmt!= null)
								stmt.close();
						} catch (Exception ignore) {
						}
						try {
							if (con != null)
								con.close();
						} catch (Exception ignore) {
						}
					}
		return users;
	}
	
	public int delete(String email) {
		Connection con = null;
		PreparedStatement ps = null;
		int returnValue = 0;

		// connexion a la base de donnees
		try {

			// tentative de connexion
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			// preparation de l'instruction SQL, le ? represente la valeur de l'ID
			// a communiquer dans la suppression.
			// le getter permet de recuperer la valeur de l'ID de l'user
			ps = con.prepareStatement("DELETE FROM inscription WHERE email = ?");
			ps.setString(1, email);

			// Execution de la requete
			returnValue = ps.executeUpdate();

		} catch (Exception e) {
			
				e.printStackTrace();
		} finally {
			// fermeture du preparedStatement et de la connexion
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}
	
	
	public int reinitialiser(String email) {
		Connection con = null;
		PreparedStatement ps = null;
		int returnValue = 0;

		// connexion a la base de donnees
		try {

			// tentative de connexion
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			// preparation de l'instruction SQL, le ? represente la valeur de l'ID
			// a communiquer dans la suppression.
			// le getter permet de recuperer la valeur de l'ID de l'user
			ps = con.prepareStatement("UPDATE inscription set valider = ? WHERE email= ?");
			ps.setInt(1, 1);
			ps.setString(2, email);

			// Execution de la requete
			returnValue = ps.executeUpdate();

		} catch (Exception e) {
			
				e.printStackTrace();
		} finally {
			// fermeture du preparedStatement et de la connexion
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}
	
	public int getId(String email) {
		Connection con = null;
		PreparedStatement ps = null;
		int returnValue=0;
		ResultSet rs = null;

		// connexion a la base de donnees
		try {

			// tentative de connexion
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			// preparation de l'instruction SQL, le ? represente la valeur de l'ID
			// a communiquer dans la suppression.
			// le getter permet de recuperer la valeur de l'ID de l'user
			ps = con.prepareStatement("SELECT Inscription_ID FROM inscription  WHERE email= ?");
			ps.setString(1, email);
			// on execute la requete
// rs contient un pointeur situe juste avant la premiere ligne retournee
		   rs = ps.executeQuery();
						// passe a la premiere (et unique) ligne retournee
						if (rs.next()) {
							returnValue =  rs.getInt("inscription_ID");
						}
					
		} catch (Exception e) {
			
				e.printStackTrace();
		} finally {
			// fermeture du preparedStatement et de la connexion
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}
}