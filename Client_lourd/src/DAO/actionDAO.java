package DAO;
import java.util.ArrayList;
import java.sql.*;
import MODEL.*;
import java.sql.Timestamp;
import java.time.LocalDateTime;

public class actionDAO extends ConnectionDAO {

	/**
	 * Constructor
	 * 
	 */
	public actionDAO() {
		super();
	}
	public ArrayList<action> getAction() throws SQLException {
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		ArrayList<action> actions = new ArrayList<>();
		try {
			// Connect to the database
			con = DriverManager.getConnection(URL, LOGIN, PASS);

			String sql = "SELECT * FROM actions";
			stmt = con.prepareStatement(sql);
			// Execute the query
			rs = stmt.executeQuery();

			// Process the results
			while (rs.next()) {
				// Create Etablissement objects and add them to the list
				action a = new action();
				a.setId_action(rs.getInt("id_action"));
				a.setId_admin(rs.getInt("id_admin"));
				a.setAction(rs.getString("action"));
				a.setId_user(rs.getString("id_user"));
				a.setDate(rs.getString("date"));
				actions.add(a);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// fermeture du rs, du preparedStatement et de la connexion
						try {
							if (rs != null)
								rs.close();
						} catch (Exception ignore) {
						}
						try {
							if (stmt!= null)
								stmt.close();
						} catch (Exception ignore) {
						}
						try {
							if (con != null)
								con.close();
						} catch (Exception ignore) {
						}
					}
		return actions;
	}
	
	public ArrayList<action> filtreAction(String action) throws SQLException {
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		ArrayList<action> actions = new ArrayList<>();
		try {
			// Connect to the database
			con = DriverManager.getConnection(URL, LOGIN, PASS);

			String sql = "SELECT * FROM actions WHERE action = ?";
			stmt = con.prepareStatement(sql);
			stmt.setString(1, action);
			
			// Execute the query
			rs = stmt.executeQuery();

			// Process the results
			while (rs.next()) {
				// Create Etablissement objects and add them to the list
				action a = new action();
				a.setId_action(rs.getInt("id_action"));
				a.setId_admin(rs.getInt("id_admin"));
				a.setAction(rs.getString("action"));
				a.setId_user(rs.getString("id_user"));
				a.setDate(rs.getString("date"));
				actions.add(a);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// fermeture du rs, du preparedStatement et de la connexion
						try {
							if (rs != null)
								rs.close();
						} catch (Exception ignore) {
						}
						try {
							if (stmt!= null)
								stmt.close();
						} catch (Exception ignore) {
						}
						try {
							if (con != null)
								con.close();
						} catch (Exception ignore) {
						}
					}
		return actions;
	}
	
	public void insertAction(int id_admin, String action, String id_user) throws SQLException {
	    // Obtenez la date et l'heure actuelles
	    LocalDateTime currentDateTime = LocalDateTime.now();
	    
	    // Convertissez LocalDateTime en Timestamp
	    Timestamp timestamp = Timestamp.valueOf(currentDateTime);
	    
	    Connection con = null;
	    PreparedStatement stmt = null;
	    try {
	        // Connexion à la base de données
	        con = DriverManager.getConnection(URL, LOGIN, PASS);
	        
	        // Requête SQL pour l'insertion d'une nouvelle action
	        String sql = "INSERT INTO actions (id_admin, action, id_user, date) VALUES (?, ?, ?, ?)";
	        stmt = con.prepareStatement(sql);
	        
	        // Définition des valeurs des paramètres dans la requête SQL
	        stmt.setInt(1, id_admin);
	        stmt.setString(2, action);
	        stmt.setString(3, id_user);
	        stmt.setTimestamp(4, timestamp); // Utilisation du Timestamp
	        
	        // Exécution de la requête
	        stmt.executeUpdate();
	        
	        System.out.println("L'action a été insérée avec succès.");

	    } catch (SQLException e) {
	        e.printStackTrace();
	    } finally {
	        // Fermeture du preparedStatement et de la connexion
	        try {
	            if (stmt != null)
	                stmt.close();
	        } catch (Exception ignore) {
	        }
	        try {
	            if (con != null)
	                con.close();
	        } catch (Exception ignore) {
	        }
	    }
	}
	
	
}
