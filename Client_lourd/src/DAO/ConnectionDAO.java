package DAO;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.Properties;



/**
 * Classe d'acces a la base de donnees
 * 
 * @author LOCATIN 
 * @version 1.0
 * */
public class ConnectionDAO {
	static String URL = "";
	static String LOGIN = "";  
	static String PASS = "";   
	
	/**
	 * Constructor
	 * 
	 */
	public ConnectionDAO() {
		// chargement du pilote de bases de donnees
		Properties prop = new Properties();
		try {
			// Charger le fichier de propriÃ©tÃ©s depuis les ressources
			InputStream inputStream = ConnectionDAO.class.getClassLoader().getResourceAsStream("config.properties");
			if (inputStream != null) {
				prop.load(inputStream);
				URL = prop.getProperty("db.url");
				LOGIN = prop.getProperty("db.username");
				PASS = prop.getProperty("db.password");
			} else {
				System.err.println("Le fichier config.properties n'a pas pu être chargé.");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			// Assurez-vous que le driver JDBC de MySQL est chargÃ©
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) throws SQLException {
		Connection con = null;
		int returnValue = 0;

		// connexion a la base de donnees
		try {

			// tentative de connexion
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			System.out.println("connexion réussi");
		} catch (Exception e) {

				e.printStackTrace();
		} finally {
			// fermeture du preparedStatement et de la connexion
			
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
	}


	
}