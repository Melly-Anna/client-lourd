package DAO;


import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import MODEL.Recherche1;



public class Recherche1DAO extends ConnectionDAO {

	private Connection connection;

    public Recherche1DAO() {
        try {
            this.connection = DriverManager.getConnection(ConnectionDAO.URL, ConnectionDAO.LOGIN, ConnectionDAO.PASS);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Recherche1> getAllRecherches() throws SQLException {
        List<Recherche1> recherches = new ArrayList<>();
        String query = "SELECT * FROM Recherche";
        try (PreparedStatement stmt = connection.prepareStatement(query)) {
            ResultSet resultSet = stmt.executeQuery();
            while (resultSet.next()) {
                Recherche1 recherche = new Recherche1(
                        resultSet.getString("idrecherche"),
                       resultSet.getDate("date"),
                        resultSet.getString("adresseip"),
                        resultSet.getString("region"),
                        resultSet.getString("codePostal"),
                        resultSet.getString("federation")
                );
                recherches.add(recherche);
            }
        }
        return recherches;
    }

    public List<Recherche1> getRecherchesByDate(String date) throws SQLException {
        List<Recherche1> recherches = new ArrayList<Recherche1>();
        String query = "SELECT * FROM Recherche WHERE DATE(date) = ?";
        try (PreparedStatement stmt = connection.prepareStatement(query)) {
            stmt.setString(1, date);
            ResultSet resultSet = stmt.executeQuery();
                while (resultSet.next()) {
                    Recherche1 recherche = new Recherche1(
                            resultSet.getString("idrecherche"),
                         resultSet.getDate("date"),
                            resultSet.getString("adresseip"),
                            resultSet.getString("region"),
                            resultSet.getString("codePostal"),
                            resultSet.getString("federation")
                    );
                    recherches.add(recherche);
                }
            }
        
        return recherches;
    }
}
