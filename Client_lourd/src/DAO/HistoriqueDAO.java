package DAO;

import MODEL.Historique;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HistoriqueDAO extends ConnectionDAO {

    private Connection connection;

    public HistoriqueDAO() {
        try {
            this.connection = DriverManager.getConnection(URL, LOGIN, PASS);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Historique> getFailedLoginAttempts() throws SQLException {
        List<Historique> failedAttempts = new ArrayList<>();
        String query = "SELECT id_users, date_connexion FROM historique WHERE statut_connexion = 'echec' AND date_connexion >= NOW() - INTERVAL 2 DAY";
        
        try (Statement stmt = connection.createStatement();
             ResultSet rs = stmt.executeQuery(query)) {
            while (rs.next()) {
                int userId = rs.getInt("id_users");
                Timestamp dateConnexion = rs.getTimestamp("date_connexion");
                failedAttempts.add(new Historique(userId, dateConnexion));
            }
        }
        
        return failedAttempts;
    }

    public Map<String, Integer> getConnectionStatsBy4HourIntervals(String startDate, String endDate) throws SQLException {
        Map<String, Integer> stats = new HashMap<>();
        String query = "SELECT HOUR(date_connexion) AS hour, COUNT(*) AS count " +
                       "FROM historique " +
                       "WHERE date_connexion BETWEEN ? AND ? " +
                       "GROUP BY HOUR(date_connexion)";

        try (PreparedStatement stmt = connection.prepareStatement(query)) {
            stmt.setString(1, startDate);
            stmt.setString(2, endDate);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                int hour = rs.getInt("hour");
                int count = rs.getInt("count");
                String interval = get4HourInterval(hour);
                stats.put(interval, stats.getOrDefault(interval, 0) + count);
            }
        }
        return stats;
    }

    private String get4HourInterval(int hour) {
        int start = (hour / 4) * 4;
        int end = start + 3;
        return String.format("%02d:00-%02d:59", start, end);
    }
    public Map<String, Integer> getSuccessFailureStats(String startDate, String endDate) throws SQLException {
        Map<String, Integer> stats = new HashMap<>();
        String query = "SELECT statut_connexion, COUNT(*) as count FROM historique " +
                       "WHERE date_connexion BETWEEN ? AND ? GROUP BY statut_connexion";

        try (PreparedStatement stmt = connection.prepareStatement(query)) {
            stmt.setString(1, startDate);
            stmt.setString(2, endDate);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                stats.put(rs.getString("statut_connexion"), rs.getInt("count"));
            }
        }
        return stats;
    }
}
