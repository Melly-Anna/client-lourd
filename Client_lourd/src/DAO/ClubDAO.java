package DAO;

import MODEL.Club;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ClubDAO extends ConnectionDAO {

    private static final String SELECT_CLUBS_BY_FILTERS = "SELECT * FROM etablissement WHERE ";
    private static final String SELECT_FEDERATIONS_BY_FILTER = "SELECT DISTINCT federation FROM etablissement WHERE ";

    public List<Club> getClubsByFilters(String filter, String value, String federation) {
        List<Club> clubs = new ArrayList<>();
        String query = SELECT_CLUBS_BY_FILTERS + filter + " LIKE ? AND federation = ?";
        try (Connection conn = DriverManager.getConnection(URL, LOGIN, PASS);
             PreparedStatement statement = conn.prepareStatement(query)) {
            // Utilisation du "%" pour gérer les cas où aucun filtre n'est appliqué
            String valueFilter = value.isEmpty() ? "%" : value;
            statement.setString(1, valueFilter);
            statement.setString(2, federation);

            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Club club = extractClubFromResultSet(resultSet);
                clubs.add(club);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return clubs;
    }

    public List<String> getAllFederations() {
        List<String> federations = new ArrayList<>();
        try (Connection conn = DriverManager.getConnection(URL, LOGIN, PASS);
             PreparedStatement statement = conn.prepareStatement("SELECT DISTINCT federation FROM etablissement");
             ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                federations.add(resultSet.getString("federation"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return federations;
    }
    
    public List<String> getAllCommunes() {
        List<String> communes = new ArrayList<>();
        try (Connection conn = DriverManager.getConnection(URL, LOGIN, PASS);
             PreparedStatement statement = conn.prepareStatement("SELECT DISTINCT commune FROM etablissement");
             ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                communes.add(resultSet.getString("commune"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return communes;
    }

    // Méthode pour récupérer toutes les régions distinctes de la base de données
    public List<String> getAllRegions() {
        List<String> regions = new ArrayList<>();
        try (Connection conn = DriverManager.getConnection(URL, LOGIN, PASS);
             PreparedStatement statement = conn.prepareStatement("SELECT DISTINCT region FROM etablissement");
             ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                regions.add(resultSet.getString("region"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return regions;
    }

    // Méthode pour récupérer tous les départements distincts de la base de données
    public List<String> getAllDepartements() {
        List<String> departements = new ArrayList<>();
        try (Connection conn = DriverManager.getConnection(URL, LOGIN, PASS);
             PreparedStatement statement = conn.prepareStatement("SELECT DISTINCT departement FROM etablissement");
             ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                departements.add(resultSet.getString("departement"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return departements;
    }

    // Méthode utilitaire pour extraire un club d'un ResultSet
    private Club extractClubFromResultSet(ResultSet resultSet) throws SQLException {
        Club club = new Club();
        club.setEtablissementId(resultSet.getInt("Etablissement_ID"));
        club.setCodeCommune(resultSet.getString("Code_Commune"));
        club.setCommune(resultSet.getString("Commune"));
        club.setDepartement(resultSet.getString("Departement"));
        club.setRegion(resultSet.getString("Region"));
        club.setStatutGeo(resultSet.getString("Statut_geo"));
        club.setCode(resultSet.getString("code"));
        club.setFederation(resultSet.getString("federation"));
        club.setClubs(resultSet.getInt("Clubs"));
        club.setEpa(resultSet.getInt("Epa"));
        club.setTotal(resultSet.getInt("Total"));
        return club;
    }
    
    
}
