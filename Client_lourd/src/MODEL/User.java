package MODEL;

public class User {
	


	private int id;
    private String nom;
    private String prenom;
    private String password;
    private String email;
    private String userType;
    private int commune;
    private String federation;
    
    
    //constructeur sans l'id
    
    public User(String nom, String prenom, String password, String email, String userType,int commune, String federation) {
  		super();
  		this.nom = nom;
  		this.prenom = prenom;
  		this.password = password;
  		this.email = email;
  		this.userType = userType;
  		this.commune=commune;
  		this.federation=federation;
  	}
    
	
	    public User(int id, String nom, String prenom, String password, String email, String userType,int commune, String federation) {
		super();
		this.id = id;
		this.nom = nom;
		this.prenom = prenom;
		this.password = password;
		this.email = email;
		this.userType = userType;
		this.commune=commune;
		this.federation=federation;
	}
	    public User( String nom, String prenom, String password, String email, String userType) {
			super();
			this.nom = nom;
			this.prenom = prenom;
			this.password = password;
			this.email = email;
			this.userType = userType;
		}
	    public User() {
			
		}
	  
	    
	    // Constructeur par défaut
	    public User(String nom2, String prenom2, String email2, String userType2) {
	    	nom2=nom;
	    	prenom2=prenom;
	    	email2=email;
	    	userType2=userType;
	    }

	    // Constructeur avec email et password pour authentification
	    public User(String email, String password) {
	        this.email = email;
	        this.password = password;
	    }

	    // Getters et setters pour chaque attribut

	    public int getId() {
	        return id;
	    }

	    public void setId(int id) {
	        this.id = id;
	    }

	    public String getNom() {
	        return nom;
	    }

	    public void setNom(String nom) {
	        this.nom = nom;
	    }

	    public String getPrenom() {
	        return prenom;
	    }

	    public void setPrenom(String prenom) {
	        this.prenom = prenom;
	    }

	    public String getPassword() {
	        return password;
	    }

	    public void setPassword(String password) {
	        this.password = password;
	    }

	    public String getEmail() {
	        return email;
	    }

	    public void setEmail(String email) {
	        this.email = email;
	    }

	    public String getUserType() {
	        return userType;
	    }

	    public void setUserType(String userType) {
	        this.userType = userType;
	    }
		public int getCommune() {
			return commune;
		}
		public void setCommune(int commune) {
			this.commune = commune;
		}
		public String getFederation() {
			return federation;
		}
		public void setFederation(String federation) {
			this.federation = federation;
		}
	    
	}

