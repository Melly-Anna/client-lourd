package MODEL;
/**
 * Modèle représentant un établissement sportif.
 * 
 * @author Melly-Anna Locatin
 * @version 1.0
 */
public class Club {

    private int etablissementId;
    private String codeCommune;
    private String commune;
    private String departement;
    private String region;
    private String statutGeo;
    private String code;
    private String federation;
    private int clubs;
    private int epa;
    private int total;

    public Club() {
    }

    public Club(String codeCommune, String commune, String departement, String region, String statutGeo, String code, String federation, int clubs, int epa, int total) {
        this.setCodeCommune(codeCommune);
        this.setCommune(commune);
        this.setDepartement(departement);
        this.setRegion(region);
        this.setStatutGeo(statutGeo);
        this.setCode(code);
        this.setFederation(federation);
        this.setClubs(clubs);
        this.setEpa(epa);
        this.setTotal(total);
    }

    
	/**
	 * @return the etablissementId
	 */
	public int getEtablissementId() {
		return etablissementId;
	}

	/**
	 * @param etablissementId the etablissementId to set
	 */
	public void setEtablissementId(int etablissementId) {
		this.etablissementId = etablissementId;
	}

	/**
	 * @return the codeCommune
	 */
	public String getCodeCommune() {
		return codeCommune;
	}

	/**
	 * @param codeCommune the codeCommune to set
	 */
	public void setCodeCommune(String codeCommune) {
		this.codeCommune = codeCommune;
	}

	/**
	 * @return the commune
	 */
	public String getCommune() {
		return commune;
	}

	/**
	 * @param commune the commune to set
	 */
	public void setCommune(String commune) {
		this.commune = commune;
	}

	/**
	 * @return the departement
	 */
	public String getDepartement() {
		return departement;
	}

	/**
	 * @param departement the departement to set
	 */
	public void setDepartement(String departement) {
		this.departement = departement;
	}

	/**
	 * @return the region
	 */
	public String getRegion() {
		return region;
	}

	/**
	 * @param region the region to set
	 */
	public void setRegion(String region) {
		this.region = region;
	}

	/**
	 * @return the statutGeo
	 */
	public String getStatutGeo() {
		return statutGeo;
	}

	/**
	 * @param statutGeo the statutGeo to set
	 */
	public void setStatutGeo(String statutGeo) {
		this.statutGeo = statutGeo;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the federation
	 */
	public String getFederation() {
		return federation;
	}

	/**
	 * @param federation the federation to set
	 */
	public void setFederation(String federation) {
		this.federation = federation;
	}

	/**
	 * @return the clubs
	 */
	public int getClubs() {
		return clubs;
	}

	/**
	 * @param clubs the clubs to set
	 */
	public void setClubs(int clubs) {
		this.clubs = clubs;
	}

	/**
	 * @return the epa
	 */
	public int getEpa() {
		return epa;
	}

	/**
	 * @param epa the epa to set
	 */
	public void setEpa(int epa) {
		this.epa = epa;
	}

	/**
	 * @return the total
	 */
	public int getTotal() {
		return total;
	}

	/**
	 * @param total the total to set
	 */
	public void setTotal(int total) {
		this.total = total;
	}

   
}
