package MODEL;

public class inscription {
	
	private int id;
	private String nom;
	private String prenom;
	private String email;
	private String type;
	private int etablissement;
	private String preuve;
	private int valider;
	private String password;

	
	public inscription(int id, String nom, String prenom, String email, String type, int etablissement, String preuve,
			int valider, String password) {
		super();
		this.id = id;
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.type = type;
		this.etablissement = etablissement;
		this.preuve = preuve;
		this.valider = valider;
		this.password = password;
		
	}

	public inscription() {
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getEtablissement() {
		return etablissement;
	}

	public void setEtablissement(int etablissement) {
		this.etablissement = etablissement;
	}

	public String getPreuve() {
		return preuve;
	}

	public void setPreuve(String preuve) {
		this.preuve = preuve;
	}

	public int getValider() {
		return valider;
	}

	public void setValider(int valider) {
		this.valider = valider;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	
	
	
	

}
