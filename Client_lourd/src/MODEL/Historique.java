package MODEL;

import java.sql.Timestamp;

public class Historique {
    private int idUser;
    private Timestamp dateConnexion;

    public Historique(int idUser, Timestamp dateConnexion) {
        this.idUser = idUser;
        this.dateConnexion = dateConnexion;
    }

    public int getIdUser() {
        return idUser;
    }

    public Timestamp getDateConnexion() {
        return dateConnexion;
    }
}
