package MODEL;

import java.sql.Date;

public class Recherche1 {
	    private String idrecherche;
	    private Date dateConnexion;
	    private String adresseIp;
	    private String region;
	    private String codePostal;
	    private String federation;

	    public Recherche1( String idrecherche, Date dateConnexion, String adresseIp, String region, String codePostal, String federation) {
	    	this.idrecherche=idrecherche;
	        this.dateConnexion = dateConnexion;
	        this.adresseIp = adresseIp;
	        this.region = region;
	        this.codePostal = codePostal;
	        this.federation = federation;
	    }

	    public String getId() {
	    	return idrecherche;
	    }

	    public Date getDateConnexion() {
	        return dateConnexion;
	    }

	    public String getAdresseIp() {
	        return adresseIp;
	    }

	    public String getRegion() {
	        return region;
	    }

	    public String getCodePostal() {
	        return codePostal;
	    }

	    public String getFederation() {
	        return federation;
	    }
}
