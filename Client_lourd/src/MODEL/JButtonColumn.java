package MODEL;
import javax.swing.*;
import javax.swing.table.*;
import java.awt.*;
import java.awt.event.*;

// Classe pour créer des boutons dans une colonne de JTable
public class JButtonColumn extends AbstractCellEditor implements TableCellRenderer, TableCellEditor, ActionListener {
    JTable table;
    Action action;

    public JButtonColumn(JTable table, Action action, int column) {
        this.table = table;
        this.action = action;

        // Ajout de l'écouteur d'action pour les boutons
        TableColumnModel columnModel = table.getColumnModel();
        columnModel.getColumn(column).setCellRenderer(this);
        columnModel.getColumn(column).setCellEditor(this);
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        JButton button = new JButton();
        button.addActionListener(this);
        button.setAction(action);
        return button;
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        return null;
    }

    @Override
    public Object getCellEditorValue() {
        return null;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        int row = table.convertRowIndexToModel(table.getEditingRow());
        fireEditingStopped();
    }
}
