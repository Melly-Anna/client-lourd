package MODEL;

public class action {
private int  id_action;
private int id_admin;
private String action;
private String id_user;
private String date;
public action(int id_action, int id_admin, String action, String id_user, String date) {
	super();
	this.id_action = id_action;
	this.id_admin = id_admin;
	this.action = action;
	this.id_user = id_user;
	this.date = date;
}
public action() {
	// TODO Auto-generated constructor stub
}
public int getId_action() {
	return id_action;
}
public void setId_action(int id_action) {
	this.id_action = id_action;
}
public int getId_admin() {
	return id_admin;
}
public void setId_admin(int id_admin) {
	this.id_admin = id_admin;
}
public String getAction() {
	return action;
}
public void setAction(String action) {
	this.action = action;
}
public String getId_user() {
	return id_user;
}
public void setId_user(String id_user) {
	this.id_user = id_user;
}
public String getDate() {
	return date;
}
public void setDate(String date) {
	this.date = date;
}


}
