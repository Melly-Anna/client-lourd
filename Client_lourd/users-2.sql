-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:8889
-- Généré le : mar. 07 mai 2024 à 10:47
-- Version du serveur : 5.7.39
-- Version de PHP : 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `club_de_sport`
--

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `userType` varchar(50) DEFAULT NULL,
  `commune` int(11) NOT NULL,
  `federation` varchar(255) DEFAULT NULL,
  `reinitialiser` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `nom`, `prenom`, `password`, `email`, `userType`, `commune`, `federation`, `reinitialiser`) VALUES
(3, 'p', 'p', 'p', 'p@p', 'Président de club', 0, '', NULL),
(10, 'w', 'w', '50e721e49c013f00c62cf59f2163542a9d8df02464efeb615d31051b0fddc326', 'w@w', 'Admin', 0, '', NULL),
(11, 'x', 'x', 'x', 'x@x', 'Admin', 0, '', NULL),
(12, 'u', 'u', '0bfe935e70c321c7ca3afc75ce0d0ca2f98b5422e008bb31c00c6d7f1f1c0ad6', 'u@u', 'Admin', 0, '', NULL),
(13, 'o', 'o', '2e7d2c03a9507ae265ecf5b5356885a53393a2029d241394997265a1a25aefc6', 'c@d', 'Entraineur', 0, '', NULL),
(14, 'aa', 'aa', 'ca978112ca1bbdcafac231b39a23dc4da786eff8147c4e72b9807785afee48bb', 'a@a', 'Admin', 0, '', NULL),
(15, 'z', 'z', 'c3641f8544d7c02f3580b07c0f9887f0c6a27ff5ab1d4a3e29caf197cfc299ae', 'z@z', 'Admin', 0, '', NULL),
(16, 'm', 'm', 'm', 'm@m', 'Admin', 1, 'fed', 1),
(17, 'v', 'v', '4c94485e0c21ae6c41ce1dfe7b6bfaceea5ab68e40a2476f50208e526f506080', 'v@v', 'Entraineur', 1, 'FF de Tennis de Table', NULL),
(18, 'ff', 'ff', '05a9bf223fedf80a9d0da5f73f5c191a665bf4a0a4a3e608f2f9e7d5ff23959c', 'ff@ff', 'Admin', 2, 'FF de Rugby', NULL),
(19, 'test', 'test', 'e3b98a4da31a127d4bde6e43033f66ba274cab0eb7eb1c70ec41402bf6273dd8', 't@t', 'Elu', 97, '', NULL),
(20, 'aaaa', 'aaaaa', '$2a$10$AjbThFkDIliyir4hKwviP.plL/iH2aHimeNvEzIL1hrWeQKEAf5V2', 'mel@a', 'Admin', 2, 'FF de Jeu de Paume', NULL),
(25, 'testtt', 'a', '$2a$10$Ilacvi1OvLvcxNI4jJTomuMypFUA5O2JdZhkYavczZu84RX0G4yX.', 'aa@aaaa', 'Elu', 3, 'Union Sportive de l\'Enseignement du Premier Degré', NULL),
(26, 'Bcrypt', 'Hashage', '$2a$10$7Br3fkgkLLRYD2fp8c0Nre3QbyxZacUM/vWcyh2I0J7Dt33CzP8Ce', 'b@c', 'Admin', 29, 'FF de Tennis de Table', NULL),
(31, 't', 't', '$2a$10$OXYjjHc2InLMf.Jf3BwXAOwyTqAZ/zZkP5vL9gReKMZqE5/CNEbTW', 'r@r', 'Admin', 0, NULL, NULL);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
